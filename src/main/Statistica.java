package main;

import java.util.LinkedList;

public class Statistica {
	private double fc,rc,ec;
	private int nrit;
	
	public class Esito {
		private double fattoreCompressione, ritardoCompressione,
				erroreCompressione;
		private int numeroRitrasmissioni;

		public Esito(double fattoreCompressione, double ritardoCompressione,
				double erroreCompressione, int numeroRitrasmissioni) {
			this.fattoreCompressione = fattoreCompressione;
			this.ritardoCompressione = ritardoCompressione;
			this.erroreCompressione = erroreCompressione;
			this.setNumeroRitrasmissioni(numeroRitrasmissioni);
		}

		public double getFattoreCompressione() {
			return fattoreCompressione;
		}

		public void setFattoreCompressione(double fattoreCompressione) {
			this.fattoreCompressione = fattoreCompressione;
		}

		public double getRitardoCompressione() {
			return ritardoCompressione;
		}

		public void setRitardoCompressione(double ritardoCompressione) {
			this.ritardoCompressione = ritardoCompressione;
		}

		public double getErroreCompressione() {
			return erroreCompressione;
		}

		public void setErroreCompressione(double erroreCompressione) {
			this.erroreCompressione = erroreCompressione;
		}

		public int getNumeroRitrasmissioni() {
			return numeroRitrasmissioni;
		}

		public void setNumeroRitrasmissioni(int numeroRitrasmissioni) {
			this.numeroRitrasmissioni = numeroRitrasmissioni;
		}
	}

	private LinkedList<Esito> tabella;

	public Statistica() {
		tabella = new LinkedList<>();
	}

	public Esito getLastEsito(){
		return tabella.getLast();
	}
	

	
	
	public void calcolaMedie(){
		for(Esito e : tabella){
			nrit=nrit+e.getNumeroRitrasmissioni();
			fc=fc+e.getFattoreCompressione();
			rc=rc+e.getRitardoCompressione();
			ec=ec+e.getErroreCompressione();
			
		}
		
		fc= fc/tabella.size();
	 rc=	rc;
	ec=	ec/tabella.size();
	}
	
	
	
	public double getFc() {
		return fc;
	}

	public double getRc() {
		return rc;
	}

	public double getEc() {
		return ec;
	}

	public int getNrit() {
		return nrit;
	}

	public void aggiungiEsito(byte[] byteInviati, byte[] byteCodificati,
			byte[] byteDecodificati, double ritardoAccumulato, int numeroRitrasmissioni) {
		double fattoreCompressione = (double) (1 - byteCodificati.length
				/ byteInviati.length)*100;
	
		double erroreCompressione = 0.0;
		int byteLen =Math.min( byteInviati.length, byteDecodificati.length);
		erroreCompressione=erroreCompressione+ Math.abs( byteInviati.length- byteDecodificati.length); //tiene conto di lunghezze diverse
		for (int i = 0; i < byteLen; i++) {
			if (byteInviati[i] != byteDecodificati[i]) {
				erroreCompressione++;
			}
		}
		erroreCompressione /= byteLen;

		tabella.add(new Esito(fattoreCompressione, ritardoAccumulato,
				erroreCompressione, numeroRitrasmissioni));
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("//=============================================STATISTICHE=============================================\\\\\n");
		sb.append("|| fattore di compressione | ritardo accumulato      | errore di compressione  | numero ritrasmissioni ||\n");
		sb.append("||=========================|=========================|=========================|=======================||\n");
		for (Esito e : tabella) {
			sb.append(String.format("||%25.3f|%25.3f|%25.3f|%23d||\n",
					e.fattoreCompressione, e.ritardoCompressione,
					e.erroreCompressione, e.numeroRitrasmissioni));
		}
		sb.append("\\\\=====================================================================================================//");
		return sb.toString();
	}
}
