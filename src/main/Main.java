package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

import canale.Canale;
import canale.CanaleSB;
import canale.CanaleWLAN;
import codifica.Codifica;
import codifica.CodificaConv;

import codifica.CodificaLZRW;
import codifica.CodificaLZW;
import codifica.CodificaPlainText;

public class Main {
	public enum tipoCod {
		LZW, LZ, CONV, PT
	};

	public enum tipoCan {
		SB, WLAN
	}

	private static int[][][] matrice1 =	{{{1, 1}, {0, 1}, {1, 1}},  //"2 ingressi, 3 uscite, 2 celle di memoria ",
			 {{0, 1}, {1, 0}, {1, 0}}};
	private static int[][][] matrice2=	{{{1, 0, 1}, {1, 1, 1}}}; // "1 ingresso, 2 uscite,3 celle di memoria", 
	private static int[][][] matrice3=	{ { {1,0,0}, {0,1,1}, {1,0,1} } };  //"1 ingresso, 3 uscite, 3 celle di memoria"
	private static int scelta(String testoDomanda, String[] testoScelte, Scanner scan) {
		int scelta = -1, numScelte = testoScelte.length, i;
		do {
			try {
				System.out.println(testoDomanda);
				for (i = 0; i < numScelte; i++) {
					System.out.printf("%d. %s\n", i, testoScelte[i]);
				}
				System.out.print("> ");
				scelta = scan.nextInt();
				if (scelta < 0 || scelta >= numScelte) {
					System.err.printf("Il numero da scegliere deve essere compreso fra 0 e %d!", numScelte-1);
				}
			} catch (InputMismatchException e) {
				System.err.println("Errore, fare una scelta con un numero!");
				System.out.println();
				continue;
			}
		} while (scelta < 0 || scelta >= numScelte);
		return scelta;
	}
	
	private static double prendiRitardo(String testo, Scanner scan) {
		double ret;
		do {
			System.out.print(testo);
			ret = scan.nextDouble();
			if (ret < 0.0) {
				System.err.println("Mettere un valore positivo!");
				System.out.println();
			}
		} while (ret < 0.0);
		return ret;
	}
	
	private static double prendiProbabilita(String testo, Scanner scan) {
		double ret;
		do {
			System.out.print(testo);
			ret = scan.nextDouble();
			if (ret < 0.0 || ret > 1.0) {
				System.err.println("Mettere un valore compreso fra 0 e 1!");
				System.out.println();
			}
		} while (ret < 0.0 || ret > 1.0);
		return ret;
	}
	
	private static double prendiSNR(String testo, Scanner scan) {
		double ret;
		
		System.out.print(testo);
		ret = scan.nextDouble();
		return ret;
	}
	
	private static int prendiNumeroFile(String testo, Scanner scan) {
		int ret;
		do {
			System.out.print(testo);
			ret = scan.nextInt();
			if (ret < 0) {
				System.err.println("Mettere un valore positivo!");
				System.out.println();
			}
		} while (ret < 0);
		return ret;
	}
	
	public static byte[] leggiFile(String filename) {
		File f = new File(filename);
		FileReader fileReader = null;
		LinkedList<Byte> listByte = null;
		int byteRead;
		byte[] ret;
		
		if (!f.exists()) {
			return null;
		}
		
		try {
			fileReader = new FileReader(f);
		} catch (FileNotFoundException e) {
			System.err.println("Non potrai mai entrare qua dentro perche' ho gia' controllato che esisti");
			System.exit(1); // semmai dovessi entrarci comunque terminati.
		}
		
		listByte = new LinkedList<>();
		while (true) {
			try {
				byteRead = fileReader.read();
				if (byteRead != -1) {
					listByte.addLast((byte) byteRead);
				} else {
					break;
				}
			} catch (IOException e) {
				// finche' non va tutto apposto riprovi sempre
			}
		}
		
		while (true) {
			try {
				fileReader.close();
				break;
			} catch (IOException e) {
				// finche' non va tutto apposto riprovi sempre
			}
		}
		
		ret = new byte[listByte.size()];
		int k = 0;
		for (Byte b : listByte) {
			ret[k++] = b; 
		}
		return ret;
	}
	
	public static void simula(byte[] inputBytes, Codifica codificaSorgente,
			Codifica codificaCanale, Canale canale, Statistica statistica) {
		double ritardoAccumulato = 0.0;
		byte[] byteCodificatiSorg = null,byteCodificatiCan, byteCanaleDecodificati = null, byteDecodificati = null;
		int numeroRitrasmissioni = 0;
		while (true) {
			byteCodificatiSorg=codificaSorgente.codifica(inputBytes);
			byteCodificatiCan = codificaCanale.codifica(byteCodificatiSorg);
			byteCanaleDecodificati = codificaCanale.decodifica(canale.invia(byteCodificatiCan));
			ritardoAccumulato += codificaSorgente.getRitardoCodifica() + codificaCanale.getRitardoCodifica() + canale.getRitardoCanale() + codificaCanale.getRitardoDecodifica();
			if (!Arrays.equals(byteCodificatiSorg, byteCanaleDecodificati)) {
				numeroRitrasmissioni++;
				continue;
			}
			byteDecodificati = codificaSorgente.decodifica(byteCanaleDecodificati); //dovrebbero essere uguali agli inputBytes
			ritardoAccumulato += codificaSorgente.getRitardoDecodifica();
			break;
		}
		statistica.aggiungiEsito(inputBytes, byteCodificatiCan, byteDecodificati, ritardoAccumulato, numeroRitrasmissioni);
	}
	
	// da usare per prove al volo
	/*
	public static void main(String [] args) {
		String daInviare = "ciao";
		Statistica stat = new Statistica();
		int num = 1;
		
		for (; num > 0; num--) {
			simula(daInviare.getBytes(), new CodificaPlainText(0.0, 0.0), new CodificaPlainText(0.0, 0.0), new CanaleWLAN(0.0, 1.0), stat);
		}
		
		System.out.println(stat);
	}*/
	
	public static void main(String[] args) {
		// dichiarazione variabili
		Codifica codificaSorgente = null, codificaCanale = null;
		Canale canale = null;
		int mrtConv[][][]=null;
		tipoCod tipoCodificaCanale = tipoCod.CONV, tipoCodificaSorgente = tipoCod.CONV;
		tipoCan tipoCanale = tipoCan.SB;
		double ritardoCodificaSorgente, ritardoCodificaCanale, ritardoCanale,
			ritardoDecodificaSorgente, ritardoDecodificaCanale;
		double ritardoPropagazione, SNR;
		Scanner scanner = new Scanner(System.in);
		int scelta = -1, numeroFile, i, byteRead;
		byte[] inputBytes = null;
		File file = null;
		FileReader fileReader = null;
		LinkedList<Byte> listByte = null;
		Statistica statistica = new Statistica();
		String filename;
		double probabilitaSingoloBit;
		
		System.out.println("===== Progetto teoria dell'informazione =====");
		System.out.println();
		
		// richiesta parametri
		do {
			scelta = scelta("Scegliere il tipo di codifica sorgente da utilizzare", new String[]{"codifica Lempel-Ziv-Welch", "Codifica Lempel-Ziv-RW", "codifica plain-text"}, scanner);
			switch (scelta) {
			case 0:
				tipoCodificaSorgente = tipoCod.LZW;
				break;
			case 1:
				tipoCodificaSorgente = tipoCod.LZ;
				break;
			case 2:
				tipoCodificaSorgente = tipoCod.PT;
				break;
			}
			ritardoCodificaSorgente = prendiRitardo("Mettere il ritardo di codifica del codificatore sorgente (in millisecondi): ", scanner);
			ritardoDecodificaSorgente = prendiRitardo("Mettere il ritardo di decodifica del codificatore sorgente (in millisecondi): ", scanner);
			
			scelta = scelta("Scegliere il tipo di codifica di canale da utilizzare", new String[]{"codifica convoluzionale", "codifica plain-text"}, scanner);
			switch (scelta) {
			case 0:
				
				tipoCodificaCanale = tipoCod.CONV;
				scelta =scelta("Si � scelta la codifica di canale Convoluzionale, scegliere uno dei polinomi ", new String[]{"2 ingressi, 3 uscite, 2 celle di memoria ", "1 ingresso, 2 uscite,3 celle di memoria", "1 ingresso, 3 uscite, 3 cella di memoria"},scanner);
				switch(scelta){
				case 0: mrtConv=matrice1; break;
				case 1: mrtConv=matrice2; break;
				case 2: mrtConv=matrice3; break;
				}
				break;
			case 1:
				tipoCodificaCanale = tipoCod.PT;
				break;
			}
			ritardoCodificaCanale = prendiRitardo("Mettere il ritardo di codifica del codificatore canale (in millisecondi): ", scanner);
			ritardoDecodificaCanale = prendiRitardo("Mettere il ritardo di decodifica del codificatore canale (in millisecondi): ", scanner);
			
			scelta = scelta("Scegliere il tipo di canale da utilizzare", new String[]{"canale simmetrico binario", "canale WLAN"}, scanner);
			switch (scelta) {
			case 0:
				tipoCanale = tipoCan.SB;
				break;
			case 1:
				tipoCanale = tipoCan.WLAN;
				break;
			}
			ritardoCanale = prendiRitardo("Mettere il ritardo di propagazione del canale (in millisecondi): ", scanner);
			
			// creazione degli oggetti canale e codifica
			switch (tipoCodificaSorgente) {
			case LZW:
				codificaSorgente = new CodificaLZW(ritardoCodificaSorgente, ritardoDecodificaSorgente);
				break;
			case LZ:
				codificaSorgente = new CodificaLZRW(ritardoCodificaSorgente, ritardoDecodificaSorgente);
				break;
			case PT:
				codificaSorgente = new CodificaPlainText(ritardoCodificaSorgente, ritardoDecodificaSorgente);
				break;
			default:
				System.out.println(tipoCodificaSorgente);
				throw new RuntimeException("WTF");
			}
			
			switch (tipoCodificaCanale) {
			case CONV:
				//scelta =scelta("Si � scelta la codifica di canale Convoluzionale, scegliere uno dei polinomi ", new String[]{"2 ingressi, 3 uscite, 2 celle di memoria ", "1 ingresso, 2 uscite,3 celle di memoria", "1 ingresso, 3 uscite, 1 cella di memoria"},scanner);
				//switch(scelta){
				//case 1:
				//codificaCanale = new CodificaConv(ritardoCodificaCanale, ritardoDecodificaCanale,matrice1); break;
				//case 2: codificaCanale=new CodificaConv(ritardoCodificaCanale, ritardoDecodificaCanale,matrice2); break;
				//case 3:  codificaCanale= new CodificaConv(ritardoCodificaCanale, ritardoDecodificaCanale,matrice3); break;
				//}
				codificaCanale = new CodificaConv(ritardoCodificaCanale, ritardoDecodificaCanale,mrtConv); 
				break;
			case PT:
				codificaCanale = new CodificaPlainText(ritardoCodificaSorgente, ritardoDecodificaSorgente);
				break;
			default:
				throw new RuntimeException("WTF");
			}
			
			switch (tipoCanale) {
			case SB:
				probabilitaSingoloBit = prendiProbabilita("Mettere la probabilita' di errore sul singolo bit del canale simmetrico binario: ", scanner);
				canale = new CanaleSB(probabilitaSingoloBit, ritardoCanale);
				break;
			case WLAN:
				SNR = prendiSNR("Mettere il signal-to-noise ratio del canale WLAN (in decibel, tra 4 e 24): ", scanner); 
				canale = new CanaleWLAN(SNR, ritardoCanale);
				break;
			default:
				System.out.println(scelta);
				System.out.println(tipoCanale);
				throw new RuntimeException("WTF");
			}
			
			// richiesta degl'input
			numeroFile = args.length;
			if (numeroFile > 0) {
				for (i = 0; i < numeroFile; i++) {
					inputBytes = leggiFile(args[i]);
					if (inputBytes == null) {
						System.err.printf("Il file \"%s\" non esiste.\n", args[i]);
						continue;
					}
					simula(inputBytes, codificaSorgente, codificaCanale, canale, statistica);
				}
				scelta = 1; // forza l'uscita
			} else {
				scelta = scelta("Vuoi inviare un file o una stringa?", new String[] {"file", "stringa"}, scanner);
				if (scelta == 0) {
					do {
						System.out.print("Inserire il nome del file da leggere: ");
						filename = scanner.next();
						inputBytes = leggiFile(filename);
						if (inputBytes == null) {
							System.err.printf("Il file \"%s\" non esiste.\n", filename);
						}
					} while (inputBytes == null);
				} else {
					System.out.print("Inserire la stringa da inviare: ");
					try {
						inputBytes = scanner.next().getBytes("UTF-8");
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				simula(inputBytes, codificaSorgente, codificaCanale, canale, statistica);
				scelta = scelta("Vuoi effettuare un altro esperimento?", new String[]{"si'", "no"}, scanner);
			} 
		} while (scelta != 1);
		
		System.out.println();
		System.out.println(statistica.toString());
		
		scanner.close();
	}
}