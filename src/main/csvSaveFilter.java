package main;



import java.io.*;
import java.io.File;
import java.util.*;
import javax.swing.filechooser.FileFilter;   
public class csvSaveFilter extends FileFilter
{ 
   public boolean accept(File f)
  {
       if (f.isDirectory())
         {
           return false;
         }

        String s = f.getName();

       return s.endsWith(".csv")||s.endsWith(".csv");
  }

  public String getDescription() 
 {
      return "*.csv,*.csv";
 }

}
