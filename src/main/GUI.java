package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import com.sun.xml.internal.ws.api.server.Container;

import canale.CanaleAstratto;
import canale.CanaleSB;
import canale.CanaleWLAN;
import codifica.CodificaAstratta;
import codifica.CodificaConv;
import codifica.CodificaLZRW;
import codifica.CodificaLZW;
import codifica.CodificaPlainText;
import main.Statistica.Esito;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.CardLayout;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.MatteBorder;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.Window.Type;
import java.awt.Dimension;
import java.util.Locale;
import java.util.Scanner;

import javax.swing.border.LineBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.event.KeyEvent;
import java.awt.SystemColor;

public class GUI extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private File files;
	private int nEsp=0;
	private int nRip=0;
	private JLayeredPane contentPane;
	private JTextField txtinserireRitardoCodSorg;
	private JTextField txtinserireRitardoDecodificaSorg;
    private CodificaAstratta codificaCanale;
    private CodificaAstratta codificaSorg;
    private CanaleAstratto canale;
	private JButton btnIniziaConversione;
	private JButton btnCancel;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton codificaTestoInputBtn;
	private JRadioButton codificaDiFileBtn;
	private JComboBox comboBox;
	private JComboBox comboBox_1;
	private JButton btnScegliFile;
	private JPanel panel_2;
	private JTextArea textArea;
	private JTextField textFileField;
	private JLabel lblTipoCanale;
	private JComboBox comboBox_2;
	private JTextField textFieldChan;
	private JTextField textFieldChan2;
	private JTextField textFieldRitChan;
	private JProgressBar progressBar1;
	private byte[] bytes;
	private int i;
	private JComboBox comboBox_Conv;
private static	 int[][][] matrice1 = new int[][][] 
			{ {{1, 1}, {0, 1}, {1, 1}} ,
			 {{0, 1}, {1, 0}, {1, 0}}  };   //"2 ingressi, 3 uscite, 2 celle di memoria ",

	private static int[][][] matrice2=	{{{1, 0, 1}, {1, 1, 1}}}; // "1 ingresso, 2 uscite,3 celle di memoria", 
	private static int[][][] matrice3=	{ { {1,0,0}, {0,1,1}, {1,0,1} } };  //"1 ingresso, 3 uscite, 3 celle di memoria"
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JTextArea textEsperimento;
	private JTextArea textFattoreCompressione;
	private JTextArea textArea_Ritardo;
	private JTextArea textArea_Errore;
	private JTextArea textArea_Ritrasmissioni;
	private Component verticalStrut;
	private JScrollPane scEx;
	private JScrollPane scFCx;
	private JScrollPane scEx2;
	private JScrollPane scExAE;
	private JScrollPane scEx3;
	private JMenuItem mEsportaRisultati;
	private JMenuItem mPulisciArea;
	private JMenuItem mAbout;
	private JComboBox comboBox_numRip;
	private Statistica sts;
	private JLabel lblNumeroRipetizioni;
	private JMenuItem mntmGuida;
	private Task task;
	private JLabel lblinserireSnr;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					GUI frame = new GUI();
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("Icona.png"));
		setTitle("Progetto Teoria Dell'Informazione");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 250, 1150, 433);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic('f');
		menuBar.add(mnFile);
		
		mEsportaRisultati = new JMenuItem("Esporta Risultati");
		mnFile.add(mEsportaRisultati);
		mEsportaRisultati.addActionListener(this);
		
		mPulisciArea = new JMenuItem("Pulisci area di lavoro");
		mnFile.add(mPulisciArea);
		mPulisciArea.addActionListener(this);
		
		JMenu mnHelp = new JMenu("Help");
		mnHelp.setMnemonic('h');
		menuBar.add(mnHelp);
		
		mntmGuida = new JMenuItem("Guida");
		mntmGuida.setIcon(null);
		mntmGuida.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		mnHelp.add(mntmGuida);
		
		mAbout = new JMenuItem("About..");
		mAbout.setIcon(null);
		mAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
		mnHelp.add(mAbout);
		mAbout.addActionListener(this);
		contentPane = new JLayeredPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(36, 15));
		panel.setMinimumSize(new Dimension(105, 13));
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		contentPane.add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] {123, 99, 84, 68, 202, 0, 0};
		gbl_panel.rowHeights = new int[] {43, 22, 22, 27, 133, 22};
		gbl_panel.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0};
		panel.setLayout(gbl_panel);
		
		JLabel lblCodificaDiSorgente = new JLabel("Codifica di Sorgente");
		lblCodificaDiSorgente.setMinimumSize(new Dimension(101, 14));
		lblCodificaDiSorgente.setMaximumSize(new Dimension(108, 14));
		GridBagConstraints gbc_lblCodificaDiSorgente = new GridBagConstraints();
		gbc_lblCodificaDiSorgente.anchor = GridBagConstraints.WEST;
		gbc_lblCodificaDiSorgente.insets = new Insets(0, 0, 5, 5);
		gbc_lblCodificaDiSorgente.gridx = 0;
		gbc_lblCodificaDiSorgente.gridy = 0;
		panel.add(lblCodificaDiSorgente, gbc_lblCodificaDiSorgente);
		
		comboBox = new JComboBox();
		comboBox.setMaximumRowCount(10);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"PlainText", "LZRW", "LZW"}));
		comboBox.setSelectedIndex(0);
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.anchor = GridBagConstraints.WEST;
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 0;
		panel.add(comboBox, gbc_comboBox);
		
		txtinserireRitardoCodSorg = new JTextField();
		txtinserireRitardoCodSorg.setText("0");
		txtinserireRitardoCodSorg.setToolTipText("[Inserire Ritardo Codifica]");
		txtinserireRitardoCodSorg.setMinimumSize(new Dimension(11, 20));
		GridBagConstraints gbc_txtinserireRitardoCodSorg = new GridBagConstraints();
		gbc_txtinserireRitardoCodSorg.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtinserireRitardoCodSorg.insets = new Insets(0, 0, 5, 5);
		gbc_txtinserireRitardoCodSorg.gridx = 2;
		gbc_txtinserireRitardoCodSorg.gridy = 0;
		panel.add(txtinserireRitardoCodSorg, gbc_txtinserireRitardoCodSorg);
		txtinserireRitardoCodSorg.setColumns(20);
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{progressBar1, menuBar, mnFile, mnHelp, contentPane, panel, lblCodificaDiSorgente, comboBox, txtinserireRitardoCodSorg, txtinserireRitardoDecodificaSorg,  btnIniziaConversione,  codificaDiFileBtn, codificaTestoInputBtn}));
		
		txtinserireRitardoDecodificaSorg = new JTextField();
		txtinserireRitardoDecodificaSorg.setText("0");
		txtinserireRitardoDecodificaSorg.setToolTipText("[Inserire Ritardo Decodifica]");
		GridBagConstraints gbc_txtinserireRitardoDecodificaSorg = new GridBagConstraints();
		gbc_txtinserireRitardoDecodificaSorg.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtinserireRitardoDecodificaSorg.insets = new Insets(0, 0, 5, 5);
		gbc_txtinserireRitardoDecodificaSorg.gridx = 3;
		gbc_txtinserireRitardoDecodificaSorg.gridy = 0;
		panel.add(txtinserireRitardoDecodificaSorg, gbc_txtinserireRitardoDecodificaSorg);
		txtinserireRitardoDecodificaSorg.setColumns(10);
		
		JLabel lblCodificaDiCanale = new JLabel("Codifica di Canale");
		lblCodificaDiCanale.setMinimumSize(new Dimension(94, 14));
		GridBagConstraints gbc_lblCodificaDiCanale = new GridBagConstraints();
		gbc_lblCodificaDiCanale.anchor = GridBagConstraints.WEST;
		gbc_lblCodificaDiCanale.insets = new Insets(0, 0, 5, 5);
		gbc_lblCodificaDiCanale.gridx = 0;
		gbc_lblCodificaDiCanale.gridy = 1;
		panel.add(lblCodificaDiCanale, gbc_lblCodificaDiCanale);
		
		comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"PlainText", "Convoluzionale"}));
		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_1.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_1.gridx = 1;
		gbc_comboBox_1.gridy = 1;
		panel.add(comboBox_1, gbc_comboBox_1);
		comboBox_1.addActionListener(this);
		
		comboBox_Conv = new JComboBox();
		comboBox_Conv.setEnabled(false);
		comboBox_Conv.setModel(new DefaultComboBoxModel(new String[] {"2 ingressi, 3 uscite, 2 celle di memoria ", "1 ingresso, 2 uscite ,3 celle di memoria", "1 ingresso, 3 uscite, 3 cella di memoria"}));
		GridBagConstraints gbc_comboBox_Conv = new GridBagConstraints();
		gbc_comboBox_Conv.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_Conv.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_Conv.gridx = 2;
		gbc_comboBox_Conv.gridy = 1;
		panel.add(comboBox_Conv, gbc_comboBox_Conv);
		
		lblinserireSnr = new JLabel("(Inserire SNR >4 e <=24)");
		GridBagConstraints gbc_lblinserireSnr = new GridBagConstraints();
		gbc_lblinserireSnr.insets = new Insets(0, 0, 5, 5);
		gbc_lblinserireSnr.gridx = 3;
		gbc_lblinserireSnr.gridy = 1;
		panel.add(lblinserireSnr, gbc_lblinserireSnr);
		
		lblTipoCanale = new JLabel("Canale");
		lblTipoCanale.setPreferredSize(new Dimension(48, 14));
		lblTipoCanale.setMinimumSize(new Dimension(68, 14));
		GridBagConstraints gbc_lblTipoCanale = new GridBagConstraints();
		gbc_lblTipoCanale.anchor = GridBagConstraints.WEST;
		gbc_lblTipoCanale.insets = new Insets(0, 0, 5, 5);
		gbc_lblTipoCanale.gridx = 0;
		gbc_lblTipoCanale.gridy = 2;
		panel.add(lblTipoCanale, gbc_lblTipoCanale);
		
		comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"Simmetrico Binario", "WLan"}));
		comboBox_2.addActionListener(this);
		GridBagConstraints gbc_comboBox_2 = new GridBagConstraints();
		gbc_comboBox_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_2.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_2.gridx = 1;
		gbc_comboBox_2.gridy = 2;
		panel.add(comboBox_2, gbc_comboBox_2);
		
		textFieldChan = new JTextField();
		textFieldChan.setText("0");
		textFieldChan.setToolTipText("[probabilit\u00E0 errore singolo bit]");
		textFieldChan.setPreferredSize(new Dimension(31, 20));
		textFieldChan.setMinimumSize(new Dimension(33, 20));
		textFieldChan.setColumns(10);
		GridBagConstraints gbc_textFieldChan = new GridBagConstraints();
		gbc_textFieldChan.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldChan.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldChan.gridx = 2;
		gbc_textFieldChan.gridy = 2;
		panel.add(textFieldChan, gbc_textFieldChan);
		
		textFieldChan2 = new JTextField();
		textFieldChan2.setEnabled(false);
		textFieldChan2.setText("0");
		textFieldChan2.setToolTipText("[SNR]");
		textFieldChan2.setColumns(10);
		GridBagConstraints gbc_textFieldChan2 = new GridBagConstraints();
		gbc_textFieldChan2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldChan2.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldChan2.gridx = 3;
		gbc_textFieldChan2.gridy = 2;
		panel.add(textFieldChan2, gbc_textFieldChan2);
		
		textFieldRitChan = new JTextField();
		textFieldRitChan.setLocale(Locale.ITALY);
		textFieldRitChan.setText("0");
		textFieldRitChan.setToolTipText("[Inserire Ritardo Canale]");
		textFieldRitChan.setMaximumSize(new Dimension(6, 20));
		GridBagConstraints gbc_textFieldRitChan = new GridBagConstraints();
		gbc_textFieldRitChan.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldRitChan.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldRitChan.gridx = 4;
		gbc_textFieldRitChan.gridy = 2;
		panel.add(textFieldRitChan, gbc_textFieldRitChan);
		textFieldRitChan.setColumns(10);
		
		lblNewLabel = new JLabel("Esperimento");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.NORTH;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 3;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Fattore Compressione (%)");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.NORTH;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 3;
		panel.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		lblNewLabel_2 = new JLabel("Ritardo Accumulato (ms)");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.NORTH;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 2;
		gbc_lblNewLabel_2.gridy = 3;
		panel.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("Errore Compressione");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.NORTH;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 3;
		gbc_lblNewLabel_3.gridy = 3;
		panel.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		lblNewLabel_4 = new JLabel("Numero Ritrasmissioni");
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.anchor = GridBagConstraints.NORTH;
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 4;
		gbc_lblNewLabel_4.gridy = 3;
		panel.add(lblNewLabel_4, gbc_lblNewLabel_4);
		
		
		textEsperimento = new JTextArea();
		scEx = new JScrollPane(textEsperimento);
		textEsperimento.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		textEsperimento.setEditable(false);
		GridBagConstraints gbc_textEsperimento = new GridBagConstraints();
		gbc_textEsperimento.insets = new Insets(0, 0, 5, 5);
		gbc_textEsperimento.fill = GridBagConstraints.BOTH;
		gbc_textEsperimento.gridx = 0;
		gbc_textEsperimento.gridy = 4;
		panel.add(scEx, gbc_textEsperimento);
		
		textFattoreCompressione = new JTextArea();
		scFCx = new JScrollPane(textFattoreCompressione);
		textFattoreCompressione.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		textFattoreCompressione.setEditable(false);
		GridBagConstraints gbc_textFattoreCompressione = new GridBagConstraints();
		gbc_textFattoreCompressione.insets = new Insets(0, 0, 5, 5);
		gbc_textFattoreCompressione.fill = GridBagConstraints.BOTH;
		gbc_textFattoreCompressione.gridx = 1;
		gbc_textFattoreCompressione.gridy = 4;
		panel.add(scFCx, gbc_textFattoreCompressione);
		
		textArea_Ritardo = new JTextArea();
		scEx2 = new JScrollPane(textArea_Ritardo);
		textArea_Ritardo.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		textArea_Ritardo.setEditable(false);
		GridBagConstraints gbc_textArea_Ritardo = new GridBagConstraints();
		gbc_textArea_Ritardo.insets = new Insets(0, 0, 5, 5);
		gbc_textArea_Ritardo.fill = GridBagConstraints.BOTH;
		gbc_textArea_Ritardo.gridx = 2;
		gbc_textArea_Ritardo.gridy = 4;
		panel.add(scEx2, gbc_textArea_Ritardo);
		
		textArea_Errore = new JTextArea();
		scExAE = new JScrollPane(textArea_Errore);
		textArea_Errore.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		textArea_Errore.setEditable(false);
		GridBagConstraints gbc_textArea_Errore = new GridBagConstraints();
		gbc_textArea_Errore.insets = new Insets(0, 0, 5, 5);
		gbc_textArea_Errore.fill = GridBagConstraints.BOTH;
		gbc_textArea_Errore.gridx = 3;
		gbc_textArea_Errore.gridy = 4;
		panel.add(scExAE, gbc_textArea_Errore);
		
		textArea_Ritrasmissioni = new JTextArea();
		scEx3 = new JScrollPane(textArea_Ritrasmissioni);
		textArea_Ritrasmissioni.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		textArea_Ritrasmissioni.setEditable(false);
		GridBagConstraints gbc_textArea_Ritrasmissioni = new GridBagConstraints();
		gbc_textArea_Ritrasmissioni.insets = new Insets(0, 0, 5, 5);
		gbc_textArea_Ritrasmissioni.fill = GridBagConstraints.BOTH;
		gbc_textArea_Ritrasmissioni.gridx = 4;
		gbc_textArea_Ritrasmissioni.gridy = 4;
		panel.add(scEx3, gbc_textArea_Ritrasmissioni);
		
		verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 0);
		gbc_verticalStrut.gridx = 5;
		gbc_verticalStrut.gridy = 4;
		panel.add(verticalStrut, gbc_verticalStrut);
		
	
		
		btnIniziaConversione = new JButton("Inizia Simulazione");
		btnIniziaConversione.setEnabled(false);
		btnIniziaConversione.setBackground(SystemColor.control);
		
		btnIniziaConversione.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_btnIniziaConversione = new GridBagConstraints();
		gbc_btnIniziaConversione.insets = new Insets(0, 0, 0, 5);
		gbc_btnIniziaConversione.gridx = 0;
		gbc_btnIniziaConversione.gridy = 5;
		panel.add(btnIniziaConversione, gbc_btnIniziaConversione);
		
		comboBox_numRip = new JComboBox();
		comboBox_numRip.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "10", "20"}));
		comboBox_numRip.setEditable(true);
		comboBox_numRip.setToolTipText("[Inserire numero ripetizioni esperimento]");
		GridBagConstraints gbc_comboBox_numRip = new GridBagConstraints();
		gbc_comboBox_numRip.insets = new Insets(0, 0, 0, 5);
		gbc_comboBox_numRip.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_numRip.gridx = 1;
		gbc_comboBox_numRip.gridy = 5;
		panel.add(comboBox_numRip, gbc_comboBox_numRip);
		
		lblNumeroRipetizioni = new JLabel("Numero Ripetizioni");
		GridBagConstraints gbc_lblNumeroRipetizioni = new GridBagConstraints();
		gbc_lblNumeroRipetizioni.anchor = GridBagConstraints.WEST;
		gbc_lblNumeroRipetizioni.insets = new Insets(0, 0, 0, 5);
		gbc_lblNumeroRipetizioni.gridx = 2;
		gbc_lblNumeroRipetizioni.gridy = 5;
		panel.add(lblNumeroRipetizioni, gbc_lblNumeroRipetizioni);
		
		
		btnCancel = new JButton("Cancella Simulazione");
		btnCancel.setEnabled(false);
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.insets = new Insets(0, 0, 0, 5);
		gbc_btnCancel.gridx = 4;
		gbc_btnCancel.gridy = 5;
		panel.add(btnCancel, gbc_btnCancel);
		btnCancel.addActionListener(this);
		btnIniziaConversione.addActionListener(this);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		contentPane.add(panel_1, BorderLayout.NORTH);
		
		codificaDiFileBtn = new JRadioButton("Codifica File");
		buttonGroup.add(codificaDiFileBtn);
		codificaDiFileBtn.addActionListener(this);
		panel_1.add(codificaDiFileBtn);
		
		btnScegliFile = new JButton("Scegli file..");
		btnScegliFile.setEnabled(false);
		btnScegliFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser jfc = new JFileChooser();
				jfc.setDialogTitle("Scegli un file");
				jfc.setMultiSelectionEnabled(true);
				jfc.showOpenDialog(panel_1);
		files=	jfc.getSelectedFile();
		textFileField.setText(files.getAbsolutePath());
			}
		});
		
		textFileField = new JTextField();
		textFileField.setEnabled(false);
		panel_1.add(textFileField);
		textFileField.setColumns(25);
		panel_1.add(btnScegliFile);
		
		codificaTestoInputBtn = new JRadioButton("Codifica testo in input");
		codificaTestoInputBtn.setHorizontalAlignment(SwingConstants.LEFT);
		buttonGroup.add(codificaTestoInputBtn);
		codificaTestoInputBtn.addActionListener(this);
		panel_1.add(codificaTestoInputBtn);
		
		progressBar1 = new JProgressBar();
		contentPane.add(progressBar1, BorderLayout.SOUTH);
		
		panel_2 = new JPanel();
		panel_2.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		contentPane.setLayer(panel_2, 1);
		contentPane.add(panel_2, BorderLayout.EAST);
		panel_2.setLayout(new CardLayout(0, 0));
		
		textArea = new JTextArea();
		textArea.setEnabled(false);
		textArea.setToolTipText("Scrivi qua il tuo testo");
		textArea.setLineWrap(true);
		textArea.setColumns(20);
		panel_2.add(textArea, "name_16725317573549");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(this.btnIniziaConversione)){
			Long time =System.currentTimeMillis();
			btnIniziaConversione.setEnabled(false);
			bytes = null;
			
			String ritCodSorg=this.txtinserireRitardoCodSorg.getText(); //TODO CONTROLLARE INPUT
			String ritDecSorg=this.txtinserireRitardoDecodificaSorg.getText();
			
			switch(this.comboBox.getSelectedIndex()){
			case 0: this.codificaSorg=new CodificaPlainText( Double.parseDouble( ritCodSorg), Double.parseDouble( ritDecSorg)); break;
			case 1: this.codificaSorg=new CodificaLZRW(Double.parseDouble( ritCodSorg), Double.parseDouble( ritDecSorg)); break;
			case 2: this.codificaSorg=new CodificaLZW(Double.parseDouble( ritCodSorg), Double.parseDouble( ritDecSorg)); break;
			}
			
			if(codificaDiFileBtn.isSelected()){
			
			bytes=Main.leggiFile(this.textFileField.getText());
								
				}//if
				
			
			else if (codificaTestoInputBtn.isSelected()){
				if(this.codificaSorg instanceof CodificaLZW)
				 bytes = this.textArea.getText().getBytes(Charset.forName("UTF-8"));
				else
					bytes = this.textArea.getText().getBytes();
				
			}//elseif
			
			if(bytes==null|| bytes.length==0){ 
				btnIniziaConversione.setEnabled(true);
				return ;}
		
			
			 ritDecSorg=ritDecSorg.replace(',', '.');
			 ritCodSorg =ritCodSorg.replace(',', '.');
			 
		
			
	
			switch(this.comboBox_1.getSelectedIndex()){
			case 0: this.codificaCanale=new CodificaPlainText(Double.parseDouble( ritCodSorg), Double.parseDouble( ritDecSorg)); break;
			
			case 1: switch(this.comboBox_Conv.getSelectedIndex()){
			
			case 0:	this.codificaCanale=new CodificaConv(Double.parseDouble( ritCodSorg), Double.parseDouble( ritDecSorg),matrice1); break;
			case 1:this.codificaCanale=new CodificaConv(Double.parseDouble( ritCodSorg), Double.parseDouble( ritDecSorg),matrice2); break;
			case 2:this.codificaCanale=new CodificaConv(Double.parseDouble( ritCodSorg), Double.parseDouble( ritDecSorg),matrice3); break;
				
			}
			}
			String probErrore=this.textFieldChan.getText();
			String snr = this.textFieldChan2.getText();
			String ritWlan=this.textFieldRitChan.getText();
			
		probErrore=	probErrore.replace(',', '.');
		snr=	snr.replace(',', '.');
		ritWlan=	ritWlan.replace(',','.');
			
			switch(this.comboBox_2.getSelectedIndex()){
			case 0: this.canale=new CanaleSB(Double.parseDouble(probErrore));break;
			case 1: this.canale=new CanaleWLAN(Double.parseDouble(snr),Double.parseDouble(ritWlan)); break;
			}
			
		
			nRip=Integer.parseInt( (String) comboBox_numRip.getSelectedItem());
			progressBar1.setValue(0);
			
			  
	
			sts = new Statistica();
	    btnCancel.setEnabled(true);
		task = new Task();
		try{
			task.start();
			
		}catch(IndexOutOfBoundsException ex){
			JOptionPane.showMessageDialog(this, "Errore totale di codifica: non � stato possibile ricostruire il messaggio originario lato ricevitore");
		}
	
		
			
		}//IniziaConversione
		
		if(e.getSource().equals(btnCancel)){
			task.interrupt();
			btnCancel.setEnabled(false);
			btnIniziaConversione.setEnabled(true);
		}
		
		if(e.getSource().equals(buttonGroup)){
			btnIniziaConversione.setEnabled(true);
		}
		if (e.getSource().equals(textFileField)){
			btnIniziaConversione.setEnabled(true);
		}
		if(e.getSource().equals(btnCancel)){
			//pulisci tutti i camp
			btnIniziaConversione.setEnabled(true);
			
		}
		if(e.getSource().equals(this.comboBox_1)){
			if(this.comboBox_1.getSelectedIndex()==1)
				comboBox_Conv.setEnabled(true);
			else
				comboBox_Conv.setEnabled(false);
		}
		if(e.getSource().equals(this.codificaDiFileBtn)){
			this.btnScegliFile.setEnabled(true);
			this.textFileField.setEnabled(true);
			this.textArea.setEnabled(false);
			this.btnIniziaConversione.setEnabled(true);
			
		}
		if(e.getSource().equals(this.codificaTestoInputBtn)){
			this.btnScegliFile.setEnabled(false);
			this.textFileField.setEnabled(false);
			this.textArea.setEnabled(true);
			this.btnIniziaConversione.setEnabled(true);
		}
		
		if(e.getSource().equals(this.comboBox_2)){
			if(this.comboBox_2.getSelectedIndex()==1){
				this.textFieldChan.setEnabled(false);
				this.textFieldChan2.setEnabled(true);}
			else{
				this.textFieldChan.setEnabled(true);
				this.textFieldChan2.setEnabled(false);}
				
		}
		
		if(e.getSource().equals(this.mAbout)){
			ImageIcon icon=new ImageIcon("DIMES-s1.png");
			JOptionPane.showMessageDialog((JFrame)this,
				    "Progetto di Teoria dell'Informazione 2017 \n \n"
				    + "Studenti:                                      Docente:\n"
				    + "Marasco Andrea                         Derango Floriano\nPasqua Francesco",
				    "About",
				    JOptionPane.INFORMATION_MESSAGE,icon);
				  
			
		}
		if(e.getSource().equals(this.mEsportaRisultati)){
			JFileChooser jfc=new JFileChooser();
			
			jfc.setDialogTitle("Esporta CSV");
			jfc.setFileFilter(new csvSaveFilter());
			jfc.setSelectedFile(new File("esperimento.csv"));
			jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			jfc.addChoosableFileFilter(new csvSaveFilter());
	int ret=		jfc.showSaveDialog(this);
		   if(ret==JFileChooser.APPROVE_OPTION){
			File f=jfc.getSelectedFile();
			String primaLinea= new String("Esperimento,Fattore Compressione, Ritardo Accumulato,Errore Compressione,Numero Ritrasmissioni ");
			Scanner s = new Scanner(textEsperimento.getText());
			Scanner s1=new Scanner(textFattoreCompressione.getText());
			Scanner s2 = new Scanner(textArea_Ritardo.getText());
			Scanner s3= new Scanner(textArea_Errore.getText());
			Scanner s4=new Scanner(textArea_Ritrasmissioni.getText());
			BufferedWriter bf;
			try {
				 bf=new BufferedWriter(new FileWriter(f));
					bf.write(primaLinea);
					bf.write("\n");
					int len=textEsperimento.getLineCount();
					while(s.hasNextLine()){
						String tmp="" + s.nextLine()+"'"+","+"'"+s1.nextLine()+"'"+","+"'"+s2.nextLine()+"'"+","+"'"+s3.nextLine()+"'"+","+"'"+s4.nextLine()+"'"+"\n";
						bf.write(tmp);
					}
					bf.close();	
					s.close();
					s1.close();
					s2.close();
					s3.close();
					s4.close();
			} catch (IOException e1) {
			
				e1.printStackTrace();
				
			}}}
		   
		   if(e.getSource().equals(mPulisciArea)){
			   
			   textEsperimento.setText("");
			   textFattoreCompressione.setText("");
			   textArea_Ritardo.setText("");
			   textArea_Errore.setText("");
			   textArea_Ritrasmissioni.setText("");
			   textFileField.setText("");
			   textFieldChan.setText("");
			   textFieldChan2.setText("");
			   textFieldRitChan.setText("");
			   txtinserireRitardoDecodificaSorg.setText("");
			   txtinserireRitardoCodSorg.setText("");
			   textArea.setText("");
		   }
		
		
		
		
		
		
		}//actionPerformed
		

	private class Task extends Thread {
		public Task(){}
		
		public void run(){
			Long time = System.currentTimeMillis();
			  progressBar1.setStringPainted(true);
			    progressBar1.setValue(0);
			    int mtu=1480;
			    progressBar1.setMaximum(nRip+(bytes.length/mtu)+10);
			    int pr=1;
			for(int y=1;y<=nRip;y++){
		
		
		
		  
		  
	     
		int	i = 0;
			//int delta=(1/(nRip+bytes.length))*100;
		
			
			if(bytes.length>mtu){
				while(i+mtu<bytes.length){
					
					Main.simula(Arrays.copyOfRange(bytes, i ,i+mtu), codificaSorg, codificaCanale, canale,sts);
					progressBar1.setValue(pr++);
					i=i+mtu;
				   
				} 
				int remaining=bytes.length-i;
				Main.simula(Arrays.copyOfRange(bytes, i ,i+remaining-1), codificaSorg, codificaCanale, canale,sts);
				progressBar1.setValue(pr++);
				
			}else{Main.simula(bytes, codificaSorg, codificaCanale, canale, sts); progressBar1.setValue(pr++);
			}
			
			}
			
			nEsp++;
			textEsperimento.append(" "+nEsp + "\n");
		    
			sts.calcolaMedie();
			progressBar1.setValue(pr+10);
		 double tempo=System.currentTimeMillis()-time;
			textFattoreCompressione.append(" "+String.format( "%3.2f",sts.getFc())+"\n");
			//textArea_Ritardo.append(" "+String.format( "%3.2f",sts.getRc()/nRip)+"\n");
			textArea_Ritardo.append(" "+String.format( "%3.2f",tempo)+"\n");
			textArea_Errore.append(" "+String.format( "%3.2f",sts.getEc())+"\n");
			textArea_Ritrasmissioni.append(" "+sts.getNrit()+"\n");
			
		
			btnIniziaConversione.setEnabled(true);
			btnCancel.setEnabled(false);
			System.out.println(System.currentTimeMillis()-time);
			}
						
		}
			
		
	
}
