package main;


import java.util.Iterator;

public class ByteArrayBitIterable implements Iterable<Boolean> {
    private  byte[] array;

    public ByteArrayBitIterable(byte[] array) {
        this.array = array;
    }

    public class Iteratore implements Iterator<Boolean>{
    	  private int bitIndex = 0;
          private int arrayIndex = 0;
          public boolean hasNext() {
              return (arrayIndex < array.length) && (bitIndex < 8);
          }

          public Boolean next() {
              Boolean val = (array[arrayIndex] >> (7 - bitIndex) & 1) == 1;
              bitIndex++;
              if (bitIndex == 8) {
                  bitIndex = 0;
                  arrayIndex++;
              }
              return val;
          }
          
          public boolean nextAndFlip(){
          	   Boolean val = (array[arrayIndex] >> (7 - bitIndex) & 1) == 1;
           int	mask = 0;
          	 
          		   switch(bitIndex){
          		   case 1: mask= 10000000; break;
          		   case 2: mask= 01000000; break;
          		   case 3: mask= 00100000; break;
          		   case 4: mask= 00010000; break;
          		   case 5: mask=00001000; break;
          		   case 6: mask=00000100; break;
          		   case 7: mask=00000010; break;
          		   case 8: mask=00000001; break;
          		   }
          		  array[arrayIndex] |=mask;
          	   
                 bitIndex++;
                 if (bitIndex == 8) {
                     bitIndex = 0;
                     arrayIndex++;
                 }
                 return val;
          	
          }
          
          public boolean flipAndNext(){
          	  
           int	mask = 0;
          	
           Boolean val = (array[arrayIndex] >> (7 - bitIndex) & 1) == 1;
           
          		   switch(bitIndex){
          		   case 7: mask= 10000000; break;
          		   case 6: mask= 01000000; break;
          		   case 5: mask= 00100000; break;
          		   case 4: mask=00010000; break;
          		   case 3: mask=00001000; break;
          		   case 2: mask=00000100; break;
          		   case 1: mask=00000010; break;
          		   case 0: mask=00000001; break;
          		   }
          		//  array[arrayIndex]=(byte) (array[arrayIndex] |mask);
          	   array[arrayIndex]=(byte)(array[arrayIndex]| 1<<(7-bitIndex));
                 bitIndex++;
                 if (bitIndex == 8) {
                     bitIndex = 0;
                     arrayIndex++;
                 }
                
                 return val;
          	
          }
          public void remove() {
              throw new UnsupportedOperationException();
          }
      };
    
      
 /*
    public Iterator<Boolean> iterator() {
        return new Iterator<Boolean>() {
            private int bitIndex = 0;
            private int arrayIndex = 0;

            public boolean hasNext() {
                return (arrayIndex < array.length) && (bitIndex < 8);
            }

            public Boolean next() {
                Boolean val = (array[arrayIndex] >> (7 - bitIndex) & 1) == 1;
                bitIndex++;
                if (bitIndex == 8) {
                    bitIndex = 0;
                    arrayIndex++;
                }
                return val;
            }
            
            public boolean nextAndFlip(){
            	   Boolean val = (array[arrayIndex] >> (7 - bitIndex) & 1) == 1;
            	int mask = 0;
            	 
            		   switch(bitIndex){
            		   case 0: mask=00000001; break;
            		   case 1: mask=00000010; break;
            		   case 2: mask=00000100; break;
            		   case 3: mask=00001000; break;
            		   case 4: mask=00010000; break;
            		   case 5: mask=00100000; break;
            		   case 6: mask=01000000; break;
            		   case 7: mask=10000000; break;
            		   }
            		  array[arrayIndex] ^=mask;
            	   
                   bitIndex++;
                   if (bitIndex == 8) {
                       bitIndex = 0;
                       arrayIndex++;
                   }
                   return val;
            	
            }
            
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }*/

    public static void main(String[] a) {
        ByteArrayBitIterable test = new ByteArrayBitIterable(
                   new byte[]{(byte)0xAA, (byte)0xAA});
        for (boolean b : test)
            System.out.println(b);
    }


	@Override
	public Iterator<Boolean> iterator() {
		
		return new Iteratore();
	}

}
