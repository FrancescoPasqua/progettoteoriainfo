package codifica;


public abstract class CodificaAstratta implements Codifica {
	protected double ritardoDecodifica, ritardoCodifica;
	
	@Override
	public double getRitardoDecodifica() {
		return this.ritardoDecodifica;
	}

	@Override
	public void setRitardoDecodifica(double ritardo) {
		this.ritardoDecodifica = ritardo;
	}

	@Override
	public double getRitardoCodifica() {
		return this.ritardoCodifica;
	}

	@Override
	public void setRitardoCodifica(double ritardo) {
		this.ritardoCodifica = ritardo;
	}

}
