package codifica.metrica;

public class MetricaCanaleSimmetricoBinario implements Metrica {
	@Override
	public int calcola(int[] ingresso, int[] uscita) {
		if (ingresso == null || uscita == null) {
			throw new IllegalArgumentException("Hai passato null");
		}
		if (ingresso.length != uscita.length) {
			throw new IllegalArgumentException("La lunghezza dei " +
					"vettori in ingresso dev'essere uguale!");
		}
		int sum = 0;
		for (int i = 0; i < ingresso.length; i++) {
			sum += ((ingresso[i] == uscita[i]) ? 1: 0);
		}
		return sum;
	}
}
