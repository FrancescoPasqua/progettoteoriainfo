package codifica.metrica;

public interface Metrica {
	int calcola(int[] ingresso, int[] uscita);
}
