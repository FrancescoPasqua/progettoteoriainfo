package codifica;


import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Set;


public class CodificaLZW extends CodificaAstratta {
	HashMap<ArrayList<Byte>,Integer> dizionario= new HashMap<>();
	HashMap<Integer,ArrayList<Byte>> diz2=new HashMap<>();
	
	public CodificaLZW(double ritardoCodifica, double ritardoDecodifica) {
		this.ritardoCodifica = ritardoCodifica;
		this.ritardoDecodifica = ritardoDecodifica;
		   initDiz2();
		   initDiz();
	}
	
	private void initDiz() {
		for(int i=0;i<=255;i++){
			ArrayList<Byte> tmp=new ArrayList<>();
			
					tmp.add( (byte) (i & 0xFF));
					
			dizionario.put( tmp,i);
					}
	}
	private void initDiz2() {
		for(int i=0;i<=255;i++){
			ArrayList<Byte> tmp=new ArrayList<>();
			
			tmp.add( (byte) (i & 0xFF));
			diz2.put(  i,tmp);
					}
		
	}
	@Override
	public byte[] codifica(byte [] x){
		Long time=System.currentTimeMillis();
	  
		ArrayList<Integer> y=new ArrayList<>();
		int j=256;
		int i=0;
	ArrayList<Byte> buffer=new ArrayList<>();
		while(i<x.length){
			buffer.add(x[i]);
			while(i<x.length-1 && condizione(buffer,x,i)   ){
				
				i++;
			}
	       		
		int s= (dizionario.get(buffer) );
		y.add(j-256, s);
		
		if(i<x.length-1){
			buffer.add(x[i+1]);
			dizionario.put( new ArrayList<>(buffer), j);}
			j++;
			i++;
			buffer.clear();
		} //Uso la classe ByteBuffer per spedire dall'altra parte gli indici del dizionario, che sono interi, e possono
		//occupare fino a 4 byte...
	
		byte[] res=new byte[y.size()*Integer.BYTES];
		for(int p=0;p<y.size(); p++ ){
			byte [] res2= ByteBuffer.allocate(Integer.BYTES).putInt(y.get(p)).array(); //trasformo indice in array di 4 byte
			for(int q=0;q<4;q++){
				
				
				res[q+p*4]=res2[q]; //tratto res come una matrice linearizzata, e gli metto i valori
			}
		
		
		}
		this.ritardoCodifica=this.ritardoCodifica+((System.currentTimeMillis()-time));
	return res;
	}
	
	private boolean condizione (ArrayList<Byte> buffer,byte[] x,int i){
		buffer.add(x[i+1]);
		if(dizionario.containsKey(buffer))
			return true;
		else{
			buffer.remove(buffer.size()-1);
			return false;
		}
		
	}
	@Override
	public byte[] decodifica(byte []input){
		
		int[] input2=new int[input.length/4];
		for(int i=0;i<input.length/4;i++){

			input2[i]=ByteBuffer.wrap(input).getInt(i*4);
		
			
		}
		Long time=System.currentTimeMillis();
	 
	    int j=255;
	    int i=0;
	    int d=0;
	    ArrayList<Byte> buffer=new ArrayList<>();
	    ArrayList<Byte> x=new ArrayList<>();
	    while(i<input2.length){
	    	if( !diz2.containsKey( input2[i])){
	    		buffer.add(buffer.get(0));
	    		diz2.put(j,new ArrayList<> (buffer));
	    		buffer.remove(buffer.size()-1);
	    	}ArrayList<Byte> tmp=diz2.get(input2[i]);
	    	int l=tmp.size();
	    
	    	for(int m=0;m<l;m++){
	    		x.add(d+m, tmp.get(m));
	    	}
	  
	    	if(condizione2(buffer,x,d)){ //verifica la presenza di buffer+x[d] nel dizionario
	    		
	    				diz2.put(j,new ArrayList<>(buffer));
	    				j++;
	    	}
	    	
	    	buffer.clear();
	    	for(Byte g: diz2.get(input2[i]))
	    	buffer.add( g);
	    	
	    			d=d+l;
	    			i++;
	    }
		byte[] res=new byte[x.size()];
		for(int p=0;p<x.size(); p++ ){
			res[p]=x.get(p).byteValue();
		}
		this.ritardoDecodifica=this.ritardoDecodifica+((System.currentTimeMillis()-time));
		
	return res;
		
	}
	
	private boolean condizione2(ArrayList<Byte> buffer, ArrayList<Byte> x, int d) {
		
		buffer.add(x.get(d));
		
		if(!this.diz2.containsKey(buffer)){
			
		return true;}
		else{
			buffer.remove(buffer.size()-1);
			return false;
		}
			
	}


public static void main(String[] args){
	CodificaLZW lzw=new CodificaLZW(0.5,0.5);
	
	Set<ArrayList<Byte>> sb= lzw.dizionario.keySet();
	
		
	String s="hammmmmmmmmmm���mmmmmminghammmmmmmm����mmmmghjjh,hj,ing";
	byte[] input=s.getBytes(Charset.forName("UTF-8"));
	
    System.out.println(Arrays.toString(input));
	
	

	byte[] tmp2=  lzw.codifica(input);
	
	byte[] output2=lzw.decodifica(tmp2);
	

	String s2=new String(output2,Charset.forName("UTF-8"));
	
	System.out.println(s2.equals(s));
	
	
	
	}




	
	//System.out.println(lzw.dizionario.toString());
	//System.out.println(lzw.diz2.toString());
	
	
}



