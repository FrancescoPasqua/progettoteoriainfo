package codifica;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import main.ByteArrayBitIterable;
import main.ByteArrayBitIterable.Iteratore;

public class CodificaLZRW extends CodificaAstratta {
	public CodificaLZRW(double ritardoCodifica, double ritardoDecodifica) {
		this.ritardoCodifica = ritardoCodifica;
		this.ritardoDecodifica = ritardoDecodifica;
	}


	final byte FLAG_COMPRESS=0x40;
	final byte FLAG_COPIED=(byte) 0x80;
	//private Byte[] hashtable;
	private int txLen;
	
	private HashMap<ArrayList<Byte>,Integer> ht;
	
	
/*	
	private boolean get_match(Byte[] hash,byte[] input, int x, int size, int pos){
		int hash_valP1=BSL(input[x], 4);
		int hash_valP2=	BXOR(hash_valP1, input[x + 1]);
		int hash_valP3=BSL(hash_valP2, 4);
		int hash_valP4=BSR(40543 * hash_valP3, input[x + 2]);
		int hash_value = BAND(hash_valP4, 0xFFF);
		int ret=0;
		if(hash[hash_value]!=-1 && (x-hash[hash_value]<4096)) {
			pos=hash[hash_value];
			size=0;
		}
		while( (x + size < input.length)&& (size<18)&& (input[x+size]==input[pos+size])){
			size++;
			
		}
		hash[hash_value]=(byte) x;
		if(size>=3)
			return true;
		else
			return false;
		
		
	
	
	}
	*/
	//int getMatch(byte[] src, int src_ptr, int inputlen, int pos ,int[] ris){
	
	
//		int size=0;
//		int key;
//	
	//	int lim;
		//byte[] match_p;
	     //if(src_ptr+2<=src.length-1){
	/*	 key = (40543*((((src[src_ptr] <<4) ^ src[src_ptr+1]) << 4) ^ src[src_ptr+2]) >> 4) & 4095;
		 if((this.hashtable[key]!=-1)&& src_ptr-this.hashtable[key]<4096){
			 pos=hashtable[key];
			 match_p=Arrays.copyOfRange(src, pos, src.length-1);
			 size=0;
			 if(src_ptr+18>this.txLen) lim=this.txLen-src_ptr;
			 else lim=18;
			 while (lim -->0 && (src[src_ptr+size]==match_p[size] ) )
				 size++;
			 
		 }
		hashtable[key]=(byte) src_ptr;} //inserisci nella hashtable ultimo pointer trovato alla seq
	ris[0]=size;
	ris[1]=pos;
	    
		return ris[0];
	}*/
	
	
	public byte[] codifica(byte[] input){ //legge tre byte per volta per trovare ricorrenze, altrimenti scrive direttamente, ogni 16 byte ci sono 2 byte (16bit, di check)
		Long time=System.currentTimeMillis();
		ht=new HashMap<>();
		this.txLen=input.length;
		byte[] control=new byte[2];
		ByteArrayBitIterable con=new ByteArrayBitIterable(control);
		Iteratore iter= (Iteratore) con.iterator();
	   
	
		
		byte[] output=new byte[input.length+1];
		output[0]=FLAG_COMPRESS;
	int	x=0;
	int	y=3;
	int	z=1;
	
	int	bit=0;
	int size=0;	
	int ris[]=new int[2];
	    while(x<input.length && y<input.length ){ //se diventa piu' lunga la stringa encodata, la copia ebbasta.
	    	//caso son passati 16byte ricreareByteArrayIterable
	    		if(bit>15){
	    		output[z]=control[0];
	    		output[z+1]=control[1];
	    		z=y;
	    		bit=0;
	    		y=y+2;
	    		control=new byte[2];
	    		 con=new ByteArrayBitIterable(control);
	    		 iter= (Iteratore) con.iterator(); 
	    		
	    			    	}
	    	
	    		size=1;
	    		while( x+size<input.length&&input[x]==input[x+size] )
	    			size++;
	    		//caso if size>16 stringhe lunghe
	    	//	if(size>=16){
	    		//	if(size-bit>0){
	    			//output[size+bit]=
	    		//	output[size+bit+1]= //nuovi control bits
	    		//	}		
	    //			iter.nextAndFlip();
	    	//		output[y+1]=(byte) x;
	    			//setta old control bits
	    		//	output[z]=control[0];
		    		//output[z+1]=control[1];
		    //		z=y+size;
		  //  		bit=0;
		    	//	y=y+2;
		    	//	control=new byte[2];
		    	//	 con=new ByteArrayBitIterable(control);
		    	//	 iter= (Iteratore) con.iterator(); 
		    	//	output[y]=(byte) size;
		    		
		    		 
	    		//	output[y] = 0;
	    		//	output[y+2] = (byte) (((size-16))&0xf);
	    		//	output[y+1] = (byte) (((size-16)>>4)&0xff);
	    		//	output[y+3] = input[x];
	    		//	y += 4;
	    		//	x += size;	
	    		//	iter.nextAndFlip();
	    	//	}
	    		//else
	    		if(contiene(input,x,size,ris) && ris[0]>2 && y+1<output.length){
	    		//esiste un match,mettici len e offset! 
	    		output[y]=	(byte)ris[0];
	    		output[y+1]=(byte)( ris[1]);	
	    	
	    		y=y+2;
	    		x=x+ris[0];
	    	
	    	   iter.flipAndNext();
	    	 
	    	
	    		
	    		}else{
	    			if(y<output.length){
	    			output[y]=input[x];
	    			y++;
	    			x++;
	    			iter.next();} else break;
	    		}
	    		bit++;	
	    		
	    		
	    }//mainwhile
	    output[z]=(byte) (control[0]);
		output[z+1]=(byte) (control[1]);
		
		if(x< input.length){ //mancano cose da scrivere sull'output
			
		}
		
		if(y<input.length){
			byte temp[]=new byte[y];
			for(int i=0;i<y;i++){
				temp[i]=output[i];
			}
			  this.ritardoCodifica=this.ritardoCodifica+((System.currentTimeMillis()-time));
			return temp;
		}
		
		
		if(y>=input.length){ //la stringa di output � venuta piu' lunga dell'input, non � stato possibile comprimerla, allora tantovale copiareh
		    for(int i=1; i<=input.length; i ++)
            {
            	output[i]=input[i-1];
            }			
	   output[0] = FLAG_COPIED;
	    y = input.length+1;
		}
		  this.ritardoCodifica=this.ritardoCodifica+((System.currentTimeMillis()-time));
		return output;
	}
	
	
	public byte[] decodifica (byte[] input){
		Long time=System.currentTimeMillis();
		byte[] output=new byte[this.txLen];
		 int x, y, bit, pos = 0,size;
		 byte[] tmp= new byte[2];
		 tmp[0]=input[1];
		 tmp[1]=input[2];
		 ByteArrayBitIterable bs= new ByteArrayBitIterable(tmp);
		 Iteratore iter = (Iteratore) bs.iterator();
		 int iterCount = 0;
		 
		 
		 if(input[0]==FLAG_COPIED){
			 output= Arrays.copyOfRange(input, 1,this.txLen+1);
			 y=input.length-1;
			 
		 }else{
			 y=0;
			 x=3;
			 boolean control=iter.next();
			
			iterCount++;
			 bit=16;
			while(x< input.length && y< this.txLen){
			if(bit==0 && x+1<input.length){
				 tmp= new byte[2];
				 tmp[0]=input[x];
				 tmp[1]=input[x+1];
				 bs= new ByteArrayBitIterable(tmp);
				 iter = (Iteratore) bs.iterator();
				 iterCount=0;				 
				x=x+2;
				bit=16;
			}
			if(x>= input.length) break;
			if(control==false && x<input.length){
				output[y]=input[x];
			y++;
			x++;
			}else{
				
				pos=input[x];
				if(pos!=0 ){
					size=input[x-1];
					if(pos+size>this.txLen){
					y=+size; continue;
					}
					 for (int k=0;k<size ;k++){
						 if(y+k-1>=0&& y+k-1<this.txLen)
                           output[y+k-1] = output[pos+k];}
					   x += 2;
					   y += size;	
				}else{
					if(iterCount>200)
					output[y]=input[x];
					y++;
					x++;}
				}//pos
			}//else
			if(iter.hasNext())
			control=iter.next();
			iterCount++;
			bit--;
			
			} 
			
		
		
		
		  this.ritardoDecodifica=this.ritardoDecodifica+((System.currentTimeMillis()-time));
		return output;
		
		
	}
	
	
	

	private boolean contiene(byte[] input, int x, int size, int[] ris) {
		ArrayList<Byte> tmp2=new ArrayList<>();
		byte[] tmp= Arrays.copyOfRange(input, x,x+ size);
		for(int i=0;i<size;i++){
			tmp2.add(tmp[i]);
		}
		
	
		if(ht.containsKey(tmp2)){
			ris[0]=tmp2.size();
			ris[1]=ht.get(tmp2);
			
			return true;
		}else{
		    
			ht.put(tmp2, x);
			ris[0]=tmp.length;
			ris[1]=x;
			return false;
		}
		
		
	}
	
	public static void main(String[] args){
		CodificaLZRW lzRw=new CodificaLZRW(0.5,0.5);
		
			
		//String s="aabbbaabbaabaaaa";
		String s="asdasdasdasddddddddddddddddddddddddddddddddddddddasddddddddddddddddddddddddddddddddd";
		byte[] input=s.getBytes();
		
		ArrayList<Byte> prova=new ArrayList<>();
		prova.add(input[0]);
	 
	    System.out.println(Arrays.toString(input));
		
		
		byte[] tmp2=  lzRw.codifica(input);
		System.out.println(Arrays.toString(tmp2));
		byte[] output2=lzRw.decodifica(tmp2);
		

	String s2=new String(output2);

	
	System.out.println(s2.equals(s));
		
		
		
		}

}
