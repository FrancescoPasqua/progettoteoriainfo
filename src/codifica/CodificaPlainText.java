package codifica;

// messa soltanto per fare dei test
public class CodificaPlainText extends CodificaAstratta {
	
	public CodificaPlainText(double ritardoCodifica, double ritardoDecodifica) {
		this.ritardoCodifica = ritardoCodifica;
		this.ritardoDecodifica = ritardoDecodifica;
	}
	
	@Override
	public byte[] codifica(byte[] input) {
		return input;
	}

	@Override
	public byte[] decodifica(byte[] input) {
		return input;
	}
	
}