package codifica;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import utils.Coppia;
import utils.nav.InsiemeNodi;
import utils.nav.Nodo;
import codifica.metrica.Metrica;
import codifica.metrica.MetricaCanaleSimmetricoBinario;

public class CodificaConv extends CodificaAstratta {
	private InsiemeNodi nodi;
	private int numeroIngressi, numeroUscite, memoriaMassimale, numNodi;
	private int numeroBlocchi;
	private Metrica metrica;
	
	public CodificaConv(double ritardoCodifica, double ritardoDecodifica,
			int[][][] matriceTrasferimento) {
		this(ritardoCodifica, ritardoDecodifica, matriceTrasferimento,
				new MetricaCanaleSimmetricoBinario());
	}
	
	public CodificaConv(double ritardoCodifica, double ritardoDecodifica,
			int[][][] matriceTrasferimento, Metrica metrica) {
		this.metrica = metrica;
		this.ritardoCodifica = ritardoCodifica;
		this.ritardoDecodifica = ritardoDecodifica;
		
		this.numeroIngressi   = matriceTrasferimento      .length;
		this.numeroUscite     = matriceTrasferimento[0]   .length;
		this.memoriaMassimale = matriceTrasferimento[0][0].length;
		
		int[] memoriaInput = new int[this.numeroIngressi];
		int modulo = -1;
		this.numNodi = 0;
		
		for (int i = 0; i < this.numeroIngressi; i++) {
			for (int j = 0; j < this.numeroUscite; j++) {
				for (int k = 0; k < this.memoriaMassimale; k++) {
					int tmp = matriceTrasferimento[i][j][k];
					if (tmp > modulo) modulo = tmp;
					if (tmp != 0 && memoriaInput[i] < k) memoriaInput[i] = k;
				}
			}
			//memoriaInput[i]++;
			this.numNodi += memoriaInput[i];
		}
		modulo++;
		
		this.numNodi = (int) Math.pow(modulo, this.numNodi);
		
		this.nodi = new InsiemeNodi(this.numNodi);
		Nodo cur = Nodo.creaNodoZero(this.numeroIngressi, modulo, memoriaInput);
		this.nodi.addNodo(cur);
		this.nodi.setNodoIniziale(cur);
		List<Nodo> daVisitare = new LinkedList<Nodo>();
		boolean[] visitato = new boolean[this.numNodi];
		daVisitare.add(cur);
		visitato[0] = true;
		
		while (!daVisitare.isEmpty()) {
			cur = daVisitare.remove(0);
			int[] adiacente = new int[this.numeroIngressi], output = null;
			
			// vengono generati TUTTI gli adiacenti
			
			// comincio a generare il primo adiacente, tutti zero
			for (int i = 0; i < this.numeroIngressi; i++) {
				adiacente[i] = 0;
			}
			
			int i = 0;
			
			// questo ciclo serve per visitare tutti gli adiacenti di un nodo
			// questa cosa corrisponde a contare un numero in base <modulo> con
			// <numeroIngressi> cifre.
			while (true) {
				output = new int[this.numeroUscite];
				int[][] statoCurr = cur.getStato(),
					statoNodoSucc = new int[this.numeroIngressi][0];
				
				for (int j = 0; j < statoCurr.length; j++) {
					int memIn = memoriaInput[j];
					int[] tmp = new int[memIn], tmp1 = statoCurr[j];
					
					tmp[0] = adiacente[j];
					for (int k = 1; k < memIn; k++) {
						tmp[k] = tmp1[k-1];
					}
					
					statoNodoSucc[j] = tmp;
				}
				
				for (int i_out = 0; i_out < this.numeroUscite; i_out++) {
					for (int i_in = 0; i_in < this.numeroIngressi; i_in++) {
						output[i_out] =
							(output[i_out] + adiacente[i_in]*
								matriceTrasferimento[i_in][i_out][0]) % modulo;
						
						for (int i_mem = 1; i_mem <= memoriaInput[i_in]; i_mem++) {
							output[i_out] = (output[i_out] +
								statoCurr[i_in][i_mem-1]*
								matriceTrasferimento[i_in][i_out][i_mem]) % modulo;
						}
					}
				}
				
				Nodo nodoSucc = this.nodi.getNodo(statoNodoSucc, modulo);
				if (nodoSucc == null) {
					nodoSucc = new Nodo(statoNodoSucc, modulo);
				}
				cur.setAdiacente(nodoSucc, adiacente, output);
				
				if (!visitato[nodoSucc.hashCode()]) {
					visitato[nodoSucc.hashCode()] = true;
					daVisitare.add(nodoSucc);
				}
				this.nodi.addNodo(nodoSucc);
				this.nodi.addNodo(cur);  //?!?
				
				// determinazione adiacente successivo
				for (i = this.numeroIngressi - 1; i >= 0; i--) {
					adiacente[i] = (adiacente[i] + 1) % modulo;
					// se non c'e' riporto allora vuol dire che e' stato generato
					// un nuovo adiacente
					// N.B.: un riporto c'e' quando una cifra torna ad essere zero
					if (adiacente[i] != 0) {
						break;
					}
				}
				
				// se il riporto e' andato oltre il numero di cifre del numero
				// allora sono stati generati tutti gli adiacenti
				if (i == -1) {
					break;
				}
			}
		}
	}

	@Override
	public byte[] codifica(byte[] input) {
		Long time=System.currentTimeMillis();
		int inputLen = input.length;
		this.numeroBlocchi = (int) Math.ceil(inputLen*8.0/this.numeroIngressi);
		byte[] ret = new byte
				[(int) Math.ceil(this.numeroBlocchi*this.numeroUscite/8.0)];
		byte retTmp = 0;
		int[] bloccoInput = new int[this.numeroIngressi];
		int i_byte = 0, mask = 128, o_byte = 0, o_pos = 7;
		for (int i_blocco = 0; i_blocco < this.numeroBlocchi; i_blocco++) {
			for (int i_bit = 0; i_bit < this.numeroIngressi; i_bit++) {
				if (i_byte >= inputLen) {
					bloccoInput[i_bit] = 0;
				} else {
					bloccoInput[i_bit] = ((mask & input[i_byte]) != 0 ? 1 : 0);
					if (mask > 1) {
						mask >>= 1;
					} else {
						mask = 128;
						i_byte++;
					}
				}
			}
			int[] output = this.nodi.naviga(bloccoInput);
			for (int i_out = 0; i_out < output.length &&
					o_byte < ret.length; i_out++) {
				ret[o_byte] |= (byte)(output[i_out] << o_pos--);
				if (o_pos < 0) {
					o_pos = 7;
					o_byte++;
				}
			}
		}
		this.ritardoCodifica=this.ritardoCodifica+((System.currentTimeMillis()-time));
		return ret;
	}
	
	private int[][] algoritmoViterbi(int[][] outputRicevutoBlocchi) {
		int numBlocchi = outputRicevutoBlocchi.length;
		int[][] ret = new int[numBlocchi][this.numeroIngressi];
		ArrayList[][] diagrammaTraliccio = new ArrayList[numBlocchi][this.numNodi];
		Nodo nodoZero = this.nodi.getNodoIniziale();
		
		for (Coppia<Nodo, int[]> c : nodoZero.getCoppieAdiacenti()) {
			Nodo n = c.getSx(); int[] output = c.getDx();
			int h_n = n.hashCode();
			ArrayList l = new ArrayList(3);
			l.add(0);
			l.add(metrica.calcola(outputRicevutoBlocchi[0], output));
			l.add(nodoZero.getInput(n));
			diagrammaTraliccio[0][h_n] = l;
		}
		
		int t = 0;
		for (; t < numBlocchi - 1; t++) {
			for (int h_t = 0; h_t < this.numNodi; h_t++) {
				if (diagrammaTraliccio[t][h_t] == null) continue;
				int metricaOld = (int) diagrammaTraliccio[t][h_t].get(1);
				Nodo cur = this.nodi.getNodo(h_t);
				for (Coppia<Nodo, int[]> c : cur.getCoppieAdiacenti()) {
					Nodo succ = c.getSx(); int[] output = c.getDx();
					ArrayList list = diagrammaTraliccio[t+1][succ.hashCode()];
					int metricaTentativo = metricaOld + metrica.calcola(outputRicevutoBlocchi[t+1], output);
					if (list == null) {
						list = new ArrayList(3);
						list.add(h_t);
						list.add(metricaTentativo);
						list.add(cur.getInput(succ));
						diagrammaTraliccio[t+1][succ.hashCode()] = list;
					} else if ((int) list.get(1) < metricaTentativo) {
						list.set(0, h_t);
						list.set(1, metricaTentativo);
						list.set(2, cur.getInput(succ));
					}
				}
			}
		}
		
		ArrayList[] ultimiNodi = diagrammaTraliccio[t];
		int maxMetrica = (int) ultimiNodi[0].get(1), maxH_t = 0;
		for (int h_t = 1; h_t < this.numNodi; h_t++) {
			if ((int) ultimiNodi[h_t].get(1) > maxMetrica) {
				maxMetrica = (int) ultimiNodi[h_t].get(1);
				maxH_t = h_t;
			}
		}
		
		for (; t >= 0; t--) {
			ret[t] = (int[]) diagrammaTraliccio[t][maxH_t].get(2);
			maxH_t = (int) diagrammaTraliccio[t][maxH_t].get(0);
		}
		
		return ret;
	}
	
	@Override
	public byte[] decodifica(byte[] input) {
		Long time=System.currentTimeMillis();
		int inputLen = input.length, i_byte = 0, mask = 128;
		int[][] blocchiOutput = new int[this.numeroBlocchi][this.numeroUscite];
		for (int i = 0, k = 0; i < this.numeroBlocchi; i++) {
			for (int j = 0; j < this.numeroUscite; j++, k++) {
				blocchiOutput[i][j] = ((mask & input[i_byte]) != 0 ? 1 : 0);
				if (mask > 1) {
					mask >>= 1;
				} else {
					mask = 128;
					i_byte++;
				}
//				mask >>= 1;
//				if (mask == 0) {
//					mask = 128;
//					i_byte++;
//				}
			}
		}
		
		int[][] inputDecodificato = algoritmoViterbi(blocchiOutput);
		
		byte[] ret = new byte[(int) Math.ceil(this.numeroBlocchi/8.0*this.numeroIngressi)];
		byte retTmp = 0;
		int i_blocco = 0, i_bit = 0;
		
		for (int i = 0; i < ret.length; i++) {
			retTmp = 0;
			for (int j = 7; j >= 0; j--) {
				retTmp |= inputDecodificato[i_blocco][i_bit++] << j;
				if (i_bit == this.numeroIngressi) {
					i_bit = 0;
					i_blocco++;
				}
			}
			ret[i] = retTmp;
		}
		this.ritardoDecodifica=this.ritardoDecodifica+((System.currentTimeMillis()-time));
		return ret;
	}

	public static void main(String[] args) {
		System.out.println(" =============== TEST ===============");
		System.out.println("Verra' provato l'esempio delle slide di Mathys " +
			"(http://ecee.colorado.edu/~mathys/ecen5682/slides/conv99.pdf) "+
			"presente a pagina 32.");
		int k = 2, n = 3, m = 2;
		int[][][] matriceTrasferimento = new int[][][] 
			{{{1, 1}, {0, 1}, {1, 1}},
			 {{0, 1}, {1, 0}, {1, 0}}};
		System.out.println("G(D) =");
		for (int i1 = 0; i1 < k; i1++) {
			System.out.print("[");
			for (int i2 = 0; i2 < n; i2++) {
				boolean scritto = false;
				for (int i3 = 0; i3 < m; i3++) {
					if (matriceTrasferimento[i1][i2][i3] != 0) {
						if (scritto) System.out.print(" + ");
						scritto = true;
						if (i3 == 0) System.out.print("1");
						else if (i3 == 1) System.out.print("D");
						else System.out.print("D^" + i3);
					}
				}
				if (i2 < n - 1) System.out.print(", ");
			}
			System.out.println("]");
		}
		System.out.print("Costruzione codec convoluzionale ...");
		CodificaConv test = new CodificaConv(0.0, 0.0, matriceTrasferimento);
		System.out.print(" OK");
		
		System.out.println();
		System.out.println("L'input che verra' testato e':");
		System.out.println("u = [00, 10, 11, 00]\n");
		int numBlocchi = 4;
		byte[] input = new byte[] {0b00101100};
		// l'input va "incastonato" in un byte
		byte[] output = null;
		
		for (int i = 0; i < input.length; i++) {
			output = test.codifica(input);
		}
		
		System.out.println("Output corrispondente:");
		System.out.print("c = [");
		int i_byte = 0, mask = 128;
		for (int i = 0; i < numBlocchi; i++) {
			for (int j = 0; j < test.numeroUscite; j++) {
				System.out.print((output[i_byte] & mask) != 0 ? 1 : 0);
				mask >>= 1;
				if (mask == 0) {
					mask = 128;
					i_byte++;
				}
			}
			if (i < numBlocchi - 1) System.out.print(", ");
		}
		System.out.println("]");
		System.out.println("Dovrebbe essere uscito;");
		System.out.println("c = [000, 101, 001, 011]");
		
		System.out.println("Prova decodifica");
		
		byte[] decodificato = test.decodifica(output);
		
		System.out.print("u = [");
		i_byte = 0; mask = 128;
		for (int i = 0; i < numBlocchi; i++) {
			for (int j = 0; j < test.numeroIngressi; j++) {
				System.out.print((decodificato[i_byte] & mask) != 0 ? 1 : 0);
				mask >>= 1;
				if (mask == 0) {
					mask = 128;
					i_byte++;
				}
			}
			if (i < numBlocchi - 1) System.out.print(", ");
		}
		System.out.println("]");
		
		System.out.println();
		
		try {
			System.out.println("Provina");
			
			//test = new CodificaConv(0.0, 0.0, new int[][][] {{{1, 0}}});
			
			input = "prova stringa".getBytes("UTF-8");
			System.out.print("input (codifica in byte): ");
			for (int i = 0; i < input.length; i++) {
				System.out.print((0x000000FF & input[i]) + " ");
			} System.out.println();
			output = test.codifica(input);
			System.out.print("input codificato con codificatore convoluzionale: ");
			for (int i = 0; i < output.length; i++) {
				System.out.print((0x000000FF & output[i]) + " ");
			} System.out.println();
			decodificato = test.decodifica(output);
			System.out.print("input decodificato: ");
			for (int i = 0; i < decodificato.length; i++) {
				System.out.print((0x000000FF & decodificato[i]) + " ");
			} System.out.println();
			System.out.println("Stringa decodificata: " + new String(decodificato, "UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(" =============== TEST ===============");
		System.out.println("Verra' provato l'esempio delle slide di Mathys " +
			"(http://ecee.colorado.edu/~mathys/ecen5682/slides/conv99.pdf) "+
			"presente a pagina 49.");
		k = 1; n = 2; m = 3;
		matriceTrasferimento = new int[][][] 
			{{{1, 0, 1}, {1, 1, 1}}};
		System.out.println("G(D) =");
		for (int i1 = 0; i1 < k; i1++) {
			System.out.print("[");
			for (int i2 = 0; i2 < n; i2++) {
				boolean scritto = false;
				for (int i3 = 0; i3 < m; i3++) {
					if (matriceTrasferimento[i1][i2][i3] != 0) {
						if (scritto) System.out.print(" + ");
						scritto = true;
						if (i3 == 0) System.out.print("1");
						else if (i3 == 1) System.out.print("D");
						else System.out.print("D^" + i3);
					}
				}
				if (i2 < n - 1) System.out.print(", ");
			}
			System.out.println("]");
		}
		System.out.print("Costruzione codec convoluzionale ...");
		test = new CodificaConv(0.0, 0.0, matriceTrasferimento);
		System.out.print(" OK");
		
		System.out.println();
		System.out.println("E' stato ricevuto il seguente output:");
		System.out.println("v = [10, 10, 00, 10, 10, 11, 01, 00]\n");
		numBlocchi = 8;
		
		output = new byte[] {
				(byte) 0b10_10_00_10,
				(byte) 0b10_11_01_00};
		// va impostato a mano il numero di blocchi perche' la classe imposta
		// il numero di blocchi a seguito di una codifica MA l'esempio in
		// questione parte da una sequenza d'input gia' codificata
		test.numeroBlocchi = numBlocchi;
		decodificato = test.decodifica(output);
		
		System.out.print("u = [");
		i_byte = 0; mask = 128;
		for (int i = 0; i < numBlocchi; i++) {
			for (int j = 0; j < test.numeroIngressi; j++) {
				System.out.print((decodificato[i_byte] & mask) != 0 ? 1 : 0);
				mask >>= 1;
				if (mask == 0) {
					mask = 128;
					i_byte++;
				}
			}
			if (i < numBlocchi - 1) System.out.print(", ");
		}
		System.out.println("]");
		System.out.println("La decodifica risultata nell'esempio indicato e':");
		System.out.println("u = [1, 1, 1, 0, 0, 1, 0, 1]");
		input = new byte[] { (byte) 0b10100111 };
		output = test.codifica(input);
	}
}