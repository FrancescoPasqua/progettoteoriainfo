package codifica;

import java.util.LinkedList;

public interface Codifica {
	/**
	 * Codifica l'array di byte.
	 * @param input	L'array di byte da codificare.
	 * @return	L'array di byte codificato.
	 */
	byte[]   codifica(byte[] input);
	
	/**
	 * Prova a decodificare l'array di byte in input.
	 * Se riesce il metodo prova a correggere l'errore altrimenti
	 * dovrebbe restituire <code>null</code>
	 * @param input	L'array di byte da decodificare.
	 * @return	L'array di byte decodificato oppure <code>null</code>
	 * 	se la decodifica non e' andata a buon fine
	 */
	byte[] decodifica(byte[] input);
	
	/**
	 * Metodo getter per il ritardo di decodifica.
	 * @return	Il ritardo di decodifica (in millisecondi).
	 */
	double getRitardoDecodifica();
	
	/**
	 * Metodo setter per il ritardo di decodifica.
	 * @param ritardo	Il ritardo di decodifica da impostare (in millisecondi).
	 */
	void setRitardoDecodifica(double ritardo);
	
	/**
	 * Metodo getter per il ritardo di codifica.
	 * @return	Il ritardo di codifica (in millisecondi).
	 */
	double getRitardoCodifica();
	
	/**
	 * Metodo setter per il ritardo di codifica.
	 * @param ritardo	Il ritardo di codifica da impostare (in millisecondi).
	 */
	void setRitardoCodifica(double ritardo);
}
