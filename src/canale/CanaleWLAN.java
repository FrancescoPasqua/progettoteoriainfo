package canale;

import utils.CatenaMarkov;


public class CanaleWLAN extends CanaleAstratto {
	private double p, P, h;
	private CatenaMarkov catenaBurst, catenaInvio;
	
	private void imposta_p(double SNR) {
		double x1 = 0.0, x2 = 0.0, y1 = 0.0, y2 = 0.0;
		if (SNR >= 0.0 && SNR < 4.1886225184092378) {
			x1 = 0.0;
			x2 = 4.1886225184092378;
			y1 = 0.8255804230887148;
			y2 = 0.9195992084506256;
		} else if (SNR >= 4.1886225184092378 && SNR < 8.1967009627491123) {
			x1 = 4.1886225184092378;
			x2 = 8.1967009627491123;
			y1 = 0.9195992084506256;
			y2 = 0.9488837481535158;
		} else if (SNR >= 8.1967009627491123 && SNR < 12.3853234811583501) {
			x1 =  8.1967009627491123;
			x2 = 12.3853234811583501;
			y1 =  0.9488837481535158;
			y2 =  0.9627553722233060;
		} else if (SNR >= 12.3853234811583501 && SNR < 16.3934019254982246) {
			x1 = 12.3853234811583501;
			x2 = 16.3934019254982246;
			y1 =  0.9627553722233060;
			y2 =  0.9658379553499260;
		} else if (SNR >= 16.3934019254982246 && SNR < 20.4736979994658483) {
			x1 = 16.3934019254982246;
			x2 = 20.4736979994658483;
			y1 =  0.9658379553499260;
			y2 =  0.9673792469132361;
		} else if (SNR >= 20.4736979994658483 && SNR < 24.5178852586195930) {
			x1 = 20.4736979994658483;
			x2 = 24.5178852586195930;
			y1 =  0.9673792469132361;
			y2 =  0.9689205384765460;
		} else if (SNR >= 24.5178852586195930 && SNR < 28.4176372585178498) {
			x1 = 24.5178852586195930;
			x2 = 28.4176372585178498;
			y1 =  0.9689205384765460;
			y2 =  0.9642966637866159;
		} else {
			throw new IllegalArgumentException();
		}
		this.p = 1 - ((y2 - y1) / (x2 - x1) * (SNR - x1) + y1);
	}
	
	private void imposta_P(double SNR) {
		double x1 = 0.0, x2 = 0.0, y1 = 0.0, y2 = 0.0;
		if (SNR >= 0.0 && SNR < 4.1886225184092378) {
			x1 = 0.0;
			x2 = 4.1886225184092378;
			y1 = 0.5358176091864321;
			y2 = 0.4864962791605116;
		} else if (SNR >= 4.1886225184092378 && SNR < 8.1967009627491123) {
			x1 = 4.1886225184092378;
			x2 = 8.1967009627491123;
			y1 = 0.4864962791605116;
			y2 = 0.5065330694835418;
		} else if (SNR >= 8.1967009627491123 && SNR < 12.3853234811583501) {
			x1 =  8.1967009627491123;
			x2 = 12.3853234811583501;
			y1 =  0.5065330694835418;
			y2 =  0.4649181972741716;
		} else if (SNR >= 12.3853234811583501 && SNR < 16.3934019254982246) {
			x1 = 12.3853234811583501;
			x2 = 16.3934019254982246;
			y1 =  0.4649181972741716;
			y2 =  0.5157808188634020;
		} else if (SNR >= 16.3934019254982246 && SNR < 20.4375891846519693) {
			x1 = 16.3934019254982246;
			x2 = 20.4375891846519693;
			y1 =  0.5157808188634020;
			y2 =  0.4556704478943114;
		} else if (SNR >= 20.4375891846519693 && SNR < 24.5178852586195930) {
			x1 = 20.4375891846519693;
			x2 = 24.5178852586195930;
			y1 =  0.4556704478943114;
			y2 =  0.3647342456590206;
		} else if (SNR >= 24.5178852586195930 && SNR < 28.3815284437039779) {
			x1 = 24.5178852586195930;
			x2 = 28.3815284437039779;
			y1 =  0.3647342456590206;
			y2 =  0.4433401153878314;
		} else {
			throw new IllegalArgumentException();
		}
		this.P = 1 - ((y2 - y1) / (x2 - x1) * (SNR - x1) + y1);
	}
	
	private void imposta_h(double SNR) {
		double x1 = 0.0, x2 = 0.0, y1 = 0.0, y2 = 0.0;
		if (SNR >= 0.0 && SNR < 4.1886225184092378) {
			x1 = 0.0;
			x2 = 4.1886225184092378;
			y1 = 0.3755232866021907;
			y2 = 0.4895788622871318;
		} else if (SNR >= 4.1886225184092378 && SNR < 8.1967009627491123) {
			x1 = 4.1886225184092378;
			x2 = 8.1967009627491123;
			y1 = 0.4895788622871318;
			y2 = 0.6467906017447531;
		} else if (SNR >= 8.1967009627491123 && SNR < 12.3853234811583501) {
			x1 =  8.1967009627491123;
			x2 = 12.3853234811583501;
			y1 =  0.6467906017447531;
			y2 =  0.6884054739541235;
		} else if (SNR >= 12.3853234811583501 && SNR < 16.3572931106843527) {
			x1 = 12.3853234811583501;
			x2 = 16.3572931106843527;
			y1 =  0.6884054739541235;
			y2 =  0.7608461774296942;
		} else if (SNR >= 16.3572931106843527 && SNR < 20.4375891846519693) {
			x1 = 16.3572931106843527;
			x2 = 20.4375891846519693;
			y1 =  0.7608461774296942;
			y2 =  0.7392680955433539;
		} else if (SNR >= 20.4375891846519693 && SNR < 24.5178852586195930) {
			x1 = 20.4375891846519693;
			x2 = 24.5178852586195930;
			y1 =  0.7392680955433539;
			y2 =  0.6837815992641935;
		} else if (SNR >= 24.5178852586195930 && SNR < 28.3815284437039779) {
			x1 = 24.5178852586195930;
			x2 = 28.3815284437039779;
			y1 =  0.6837815992641935;
			y2 =  0.7438919702332840;
		} else {
			throw new IllegalArgumentException("Valore di SNR non contemplato.");
		}
		this.h = (y2 - y1) / (x2 - x1) * (SNR - x1) + y1;
	}
	
	public CanaleWLAN(double SNR, double ritardoCanale) {
		double[][] matriceBurstiness;
		double[][] matriceInvio;
		
		imposta_p(SNR);
		imposta_P(SNR);
		imposta_h(SNR);
		
		matriceBurstiness = new double[][] {{1.0-P, P}, {p, 1.0-p}};
		matriceInvio = new double[][] {{h, 1.0-h}, {1.0-h, h}};
		this.ritardoCanale = ritardoCanale;
		this.catenaBurst = new CatenaMarkov(matriceBurstiness, 0);
		this.catenaInvio = new CatenaMarkov(matriceInvio, 0);
	}

	@Override
	public byte[] invia(byte[] input) {
		byte[] output = new byte[input.length];
		int byteLen = input.length;
		byte curByte, flip = 0;
		
		for (int i = 0; i < byteLen; i++) {
			curByte = input[i];
			// questo for flippa il j-esimo bit del byte i-esimo
			// fare uno XOR fra un bit ed uno equivale a flippare il bit
			// mentre fare lo XOR con uno zero lascia il bit (byte) inalterato
			// inoltre 1<<j restituisce 2^j che e' 1 in posizione j
			for (int j = 0; j < 8; j++) {
				if (this.catenaBurst.prossimoNodo(Math.random()) == 1) {
					flip = (byte) this.catenaInvio.prossimoNodo(Math.random());
				} else {
					flip = 0;
				}
				curByte ^= flip << j;
			}
			output[i] = curByte;
		}
		return output;
	}

}