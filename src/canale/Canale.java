package canale;

public interface Canale {
	/**
	 * Metodo per simulare l'invio e l'errore sul canale
	 * @param input	L'array di byte da inviare.
	 * @return	L'array di byte ricevuto.
	 */
	byte[] invia(byte[] input);
	
	/**
	 * Metodo getter per il ritardo di propagazione sul canale.
	 * @return	Il ritardo di propagazione sul canale (in millisecondi).
	 */
	double getRitardoCanale();
	
	/**
	 * Metodo setter per il ritardo di propagazione sul canale.
	 * @param ritardo	Il ritardo di propagazione sul canale da impostare (in millisecondi).
	 */
	void setRitardoCanale(double ritardo);
}
