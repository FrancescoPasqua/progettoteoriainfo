package canale;

public class CanaleSB extends CanaleAstratto {
	private double probabilitaErrore;

	public CanaleSB(double probabilitaErrore) {
		this.probabilitaErrore = probabilitaErrore;
	}
	
	public CanaleSB(double probabilitaErrore, double ritardoCanale) {
		this.probabilitaErrore = probabilitaErrore;
		this.ritardoCanale = ritardoCanale;
	}

	@Override
	public byte[] invia(byte[] input) {
		byte[] output = new byte[input.length];
		int byteLen = input.length;
		byte curByte;
		for (int i = 0; i < byteLen; i++) {
			curByte = input[i];
			// questo for flippa il j-esimo bit del byte i-esimo
			// fare uno XOR fra un bit ed uno equivale a flippare il bit
			// mentre fare lo XOR con uno zero lascia il bit (byte) inalterato
			// inoltre 1<<j restituisce 2^j che e' 1 in posizione j
			for (int j = 0; j < 8; j++) {
				curByte ^= (Math.random() <= this.probabilitaErrore) ? (1 << j) : 0;
			}
			output[i] = curByte;
		}
		return output;
	}
}

