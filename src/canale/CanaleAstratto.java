package canale;

public abstract class CanaleAstratto implements Canale {
	protected double ritardoCanale;
	
	
	@Override
	public double getRitardoCanale() {
		return this.ritardoCanale;
	}
	
	@Override
	public void setRitardoCanale(double ritardo) {
		this.ritardoCanale = ritardo;
	}
}
