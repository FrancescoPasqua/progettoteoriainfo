package utils;

import java.util.ArrayList;
import java.util.LinkedList;

public class CatenaMarkov {
	private class Coppia<T1, T2> {
		private T1 sx;
		private T2 dx;
		
		public Coppia(T1 sx, T2 dx) {
			this.sx = sx;
			this.dx = dx;
		}
		
		public T1 getSx() {
			return sx;
		}
		
		public T2 getDx() {
			return dx;
		}
	}
	
	private ArrayList<LinkedList<Coppia<Integer, Double>>> catena;
	private int nodoCorrente;
	
	public CatenaMarkov(double[][] matriceAdiacenza, int nodoPartenza) {
		LinkedList<Coppia<Integer, Double>> adiacentiCorrente;
		double sommaControllo, pesoCorrente;
		int i, j, n;
		
		// controlli d'integrita' matrice
		if (matriceAdiacenza == null ||
				matriceAdiacenza.length != matriceAdiacenza[0].length) {
			throw new IllegalArgumentException(
					"Matrice di adiacenza nulla o non quadrata");
		}
		
		n = matriceAdiacenza.length;
		
		if (nodoPartenza < 0 || nodoPartenza >= n) {
			throw new IllegalArgumentException(
				"Il nodo di partenza e' superiore al numero di nodi presenti" +
				" nella catena di Markov!");
		}
		
		for (i = 0; i < n; i++) {
			sommaControllo = 0.0;
			for (j = 0; j < n; j++) {
				pesoCorrente = matriceAdiacenza[i][j];
				if (pesoCorrente < 0.0 || pesoCorrente > 1.0) {
					throw new IllegalArgumentException(
						String.format("Peso in posizione (%d, %d) non valido.",
								i, j));
				}
				
				sommaControllo += pesoCorrente;
			}
			if (sommaControllo != 1.0) {
				throw new IllegalArgumentException(
					String.format(
						"La somma delle probabilita' sulla riga " +
						"%d non fa 1!", i));
			}
		}
		
		// creazione struttura dati catena di Markov
		catena = new ArrayList<LinkedList<Coppia<Integer, Double>>>(n);
		for (i = 0; i < n; i++) {
			adiacentiCorrente = new LinkedList<Coppia<Integer, Double>>();
			for (j = 0; j < n; j++) {
				pesoCorrente = matriceAdiacenza[i][j];
				if (pesoCorrente > 0.0) {
					adiacentiCorrente
						.add(new Coppia<Integer, Double>(j, pesoCorrente));
				}
			}
			catena.add(adiacentiCorrente);
		}
		
		this.nodoCorrente = nodoPartenza;
	}
	
	public int prossimoNodo(double numeroEstratto) {
		LinkedList<Coppia<Integer, Double>> adiacentiCorrente;
		double probabilitaCumulata = 0.0;
		
		if (numeroEstratto < 0.0 || numeroEstratto > 1.0) {
			throw new IllegalArgumentException("Il numero casuale passato e'" +
					" superiore di 1!");
		}
		
		adiacentiCorrente = catena.get(nodoCorrente);
		for (Coppia<Integer, Double> adiacente : adiacentiCorrente) {
			probabilitaCumulata += adiacente.getDx();
			if (numeroEstratto <= probabilitaCumulata) {
				this.nodoCorrente = adiacente.getSx();
				return this.nodoCorrente;
			}
		}
		
		throw new RuntimeException("Questa cosa non dovrebbe M - A - I accadere " +
				"poiche' numeroEstratto dovrebbe essere compreso fra 0 e 1" +
				" e se accade allora e' fallita la if in riga 89! (lol)");
	}
}
