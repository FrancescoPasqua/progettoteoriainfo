package utils.nav;

import java.util.Arrays;

import utils.Coppia;

public class Nodo {
	private int[][] stato;
	private int statoValore, base, numNodi;
	private Coppia<Nodo, int[]>[] adiacenti;
	private int[][] nodoInput;
	
	private int hash(int[] vettore) {
		int ret = 0, multiplier = 1;
		for (int i = vettore.length-1; i >= 0; i--) {
			ret += vettore[i]*multiplier;
			multiplier *= this.base;
		}
		return ret;
	}
	
	public static int hash(int[][] stato, int modulo) {
		int ret = 0, multiplier = 1;
		for (int i = 0; i < stato.length; i++) {
			for (int j = 0; j < stato[i].length; j++) {
				ret += stato[i][j]*multiplier;
				multiplier *= modulo;
			}
		}
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	public Nodo(int[][] stato, int modulo) {
		int multiplier = 1, numIngressi = stato.length,
				numAdiacenti = (int) Math.pow(modulo, numIngressi);
		
		this.statoValore = 0;
		this.stato = stato;
		this.base = modulo;
		this.adiacenti = new Coppia[numAdiacenti];
		this.numNodi = 0;
		
		for (int i = 0; i < numIngressi; i++) {
			for (int j = 0; j < stato[i].length; j++) {
				this.statoValore += stato[i][j]*multiplier;
				multiplier *= modulo;
			}
			this.numNodi += stato[i].length;
		}
		this.numNodi = (int) Math.pow(modulo, this.numNodi);
		
		this.nodoInput = new int[this.numNodi][numIngressi];
	}
	
	public Coppia<Nodo, int[]>[] getCoppieAdiacenti() {
		return this.adiacenti;
	}
	
	public int[][] getStato() {
		return this.stato;
	}
	
	public int[] getInput(Nodo n) {
		return this.nodoInput[n.hashCode()];
	}
	
	public Coppia<Nodo, int[]> getNodoEOutput(int[] input) {
		return this.adiacenti[this.hash(input)];
	}
	
	public static Nodo creaNodoZero(int k, int q, int[] m) {
		int[][] zero = new int[k][0];
		
		for (int i = 0; i < k; i++) {
			int p = m[i];
			zero[i] = new int[p];
			for (int j = 0; j < p; j++) {
				zero[i][j] = 0;
			}
		}
		
		return new Nodo(zero, q);
	}
	
	public void setAdiacente(Nodo nodoSucc, int[] input, int[] output) {
		int hashInput = this.hash(input);
		this.adiacenti[hashInput] = new Coppia<Nodo, int[]>(nodoSucc, output);
		this.nodoInput[nodoSucc.hashCode()] = Arrays.copyOf(input, input.length);
		
	}
	
	@Override
	public int hashCode() {
		return this.statoValore;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (o == this) return true;
		if (!(o instanceof Nodo)) return false;
		Nodo altro = (Nodo) o;
		return this.statoValore == altro.statoValore;
	}
	
	public static void main(String[] args) {
		int[][] x = new int[3][0];
		x[0] = new int[2];
		x[1] = new int[1];
		x[2] = new int[4];
		
		x[2][2] = 3;
		for (int i = 0; i < x.length; i++) {
			for (int j = 0; j < x[i].length; j++) {
				System.out.print(x[i][j] + " ");
			}
			System.out.println();
		}
		
		int y = (int) Math.ceil(3*8.0/5);
		double z = 3*8.0/5;
		System.out.println(y);
		System.out.println(z);
	}
	
}
