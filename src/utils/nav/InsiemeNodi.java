package utils.nav;

import utils.Coppia;

public class InsiemeNodi {
	private Nodo nodoIniziale, nodoCorrente;
	private Nodo[] nodi;
	
	public InsiemeNodi(int numNodi) {
		this.nodi = new Nodo[numNodi];
	}
	
	public void addNodo(Nodo n) {
		this.nodi[n.hashCode()] = n;
	}
	
	public Nodo getNodoIniziale() {
		return this.nodoIniziale;
	}
	
	public Nodo getNodo(int hash) {
		return this.nodi[hash];
	}
	
	public boolean hasNodo(int hash) {
		return this.nodi[hash] != null;
	}
	
	public boolean hasNodo(Nodo n) {
		return this.nodi[n.hashCode()] != null;
	}
	
	public Nodo getNodo(int[][] stato, int modulo) {
		return this.nodi[Nodo.hash(stato, modulo)];
	}
	
	public boolean hasNodo(int[][] stato, int modulo) {
		return this.getNodo(stato, modulo) != null;
	}
	
	public void setNodoIniziale(Nodo n) {
		this.nodoIniziale = n;
		this.nodoCorrente = n;
	}
	
	public int[] naviga(int[] input) {
		Coppia<Nodo, int[]> dest = this.nodoCorrente.getNodoEOutput(input);
		this.nodoCorrente = dest.getSx();
		return dest.getDx();
	}
}
