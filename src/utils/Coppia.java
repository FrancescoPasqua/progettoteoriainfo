package utils;

public class Coppia<T1, T2> {
	private T1 sx;
	private T2 dx;
	
	public Coppia(T1 sx, T2 dx) {
		this.sx = sx;
		this.dx = dx;
	}
	
	public T1 getSx() {
		return sx;
	}
	
	public T2 getDx() {
		return dx;
	}
	
	@Override
	public String toString() {
		return "<" + sx + ", " + dx + ">";
	}
}